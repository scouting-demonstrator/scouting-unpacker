#Reminder of compile command
#c++ -fPIC -std=c++11 -I /home/tjames/tf2_gpu_env/include/python3.7m/ -o scunpack.so -shared -L /home/tjames/tf2_gpu_env/lib -lpython3.7m skimAndConvert.cc
#I had to copy libpython3.7m.so.1.0 locally
#conda activate tf2_22_gpu

import sys
import math
import os
import datetime
sys.path.insert(0, '/opt/scunpack/site-packages')
import scunpack
import pandas as pd
import numpy as np
import ctypes
from numpy.ctypeslib import ndpointer
import json

file_gmt = sys.argv[1]
file_calo = sys.argv[2]
dir = sys.argv[3]
start = int(sys.argv[4])
end = int(sys.argv[5])

#set defaults
floats = False
excludeIntermediate_=True
excludeFinal_=False

#setup
run_number_calo=file_calo[11:17]
run_number_gmt=file_gmt[10:16]
print("run number calo:" + run_number_calo)
print("run number GMT:" + run_number_gmt)
time = datetime.datetime.now().strftime("%c")

#adictionary_calo_floats=scunpack.convertcalo(dir + file_calo, start, end, wantDict=True, wantFloats=True)

#df_floats = pd.DataFrame.from_dict(adictionary_calo_floats)

#del adictionary_calo_floats

#df_floats.to_json('/home/tjames/scout_CALO_' + run_number_calo + '_' + str(start)[0] + '.json')

#del df_floats

if(str(start)[0] == '0'):
    adictionary_gmt=scunpack.convert(dir + file_gmt, 0, 1, wantDict=True,wantFloats=True,excludeIntermediate=excludeIntermediate_,excludeFinal=excludeFinal_)

    df_gmt = pd.DataFrame.from_dict(adictionary_gmt)

    del adictionary_gmt

    df_gmt.to_json('/home/tjames/scout_GMT_' + run_number_gmt + '_' + str(start)[0] + '.json')

print("finished job " + str(start)[0] + " with " + df_gmt["bytes"])
