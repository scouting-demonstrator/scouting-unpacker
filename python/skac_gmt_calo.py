#Reminder of compile command
#c++ -fPIC -std=c++11 -I /home/tjames/tf2_gpu_env/include/python3.7m/ -o scunpack.so -shared -L /home/tjames/tf2_gpu_env/lib -lpython3.7m skimAndConvert.cc
#I had to copy libpython3.7m.so.1.0 locally
#conda activate tf2_22_gpu

import sys
import math
import os
import datetime
sys.path.insert(0, '/opt/scunpack/site-packages')
import matplotlib.pyplot as plt
from matplotlib import colors, rcParams
from matplotlib.ticker import PercentFormatter
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.backends.backend_agg
import scunpack
import pandas as pd
import numpy as np
import ctypes
from numpy.ctypeslib import ndpointer
import json
from threading import Thread
import collections

nEventsCalo=100000000
nEventsMuon=10000000

class ThreadWithReturnValue(Thread):
    def __init__(self, group=None, target=None, name=None, args=(), kwargs=None, *, daemon=None):
        Thread.__init__(self, group, target, name, args, kwargs, daemon=daemon)

        self._return = None

    def run(self):
        if self._target is not None:
            self._return = self._target(*self._args, **self._kwargs)

    def join(self):
        Thread.join(self)
        return self._return

file_gmt = sys.argv[1]
file_calo = sys.argv[2]
nevents = int(sys.argv[3])
dir = sys.argv[4]

#set defaults
floats = False
excludeIntermediate_=True
excludeFinal_=False

#setup
run_number_calo=file_calo[11:17]
run_number_gmt=file_gmt[10:16]
file_number_gmt=file_gmt[18:23]
file_number_calo=file_calo[19:24]
print("run number calo:" + run_number_calo)
print("run number GMT:" + run_number_gmt)
time = datetime.datetime.now().strftime("%c")
matplotlib.rc('figure', dpi = '300')

def adict(wantFloats_):
    result=scunpack.convertcalo(dir + file_calo, 0, nEventsCalo, wantDict=True, wantFloats=wantFloats_)
    return result
    print("convert calo file done, wantFloats = " + wantFloats_)
    

t1 = ThreadWithReturnValue(target=adict, args=(1,))
t2 = ThreadWithReturnValue(target=adict, args=(0,))
t1.start()
t2.start()

adictionary_calo_nofloats=t2.join()
adictionary_calo_floats=t1.join()
    
df = pd.DataFrame.from_dict(adictionary_calo_nofloats)
df_floats = pd.DataFrame.from_dict(adictionary_calo_floats)

del adictionary_calo_nofloats
del adictionary_calo_floats

df_floats.drop('orbit', axis=1, inplace=True)
df_floats['orbit']=df['orbit']

objects="jets"

if(run_number_calo != run_number_gmt): 
 print("Warning GMT and Calo run numbers do not match")

os.chdir("/home/scouter/scunpack/")

plot_path="/home/scouter/scunpack/plots/plots_"

if not (os.path.exists(plot_path + run_number_calo + "_" + file_number_calo)):
 os.mkdir(plot_path + run_number_calo + "_" + file_number_calo)
 os.mkdir(plot_path + run_number_calo + "_" + file_number_calo + "/muons")
 os.mkdir(plot_path + run_number_calo + "_" + file_number_calo + "/jets")
 os.mkdir(plot_path + run_number_calo + "_" + file_number_calo + "/taus")
 os.mkdir(plot_path + run_number_calo + "_" + file_number_calo + "/egammas")
 os.mkdir(plot_path + run_number_calo + "_" + file_number_calo + "/esums")

#Type
fig, axs = plt.subplots(1, 1)
plt.xlim([0,4])
axs.hist(df['type'], bins=4)
axs.set_ylabel('occupancy')
axs.set_xlabel('Type number')
labels = [item.get_text() for item in axs.get_xticklabels()]
labels[1] = 'jets'
labels[2] = 'egammas'
labels[4] = 'esums'
labels[5] = 'taus'
axs.set_xticklabels(labels)
plt.title(str(file_calo) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
plt.savefig(plot_path + run_number_calo + "_" + file_number_calo +'/jets/Type.png')
plt.savefig(plot_path + run_number_calo + "_" + file_number_calo +'/egammas/Type.png')
plt.savefig(plot_path + run_number_calo + "_" + file_number_calo +'/taus/Type.png')
plt.savefig(plot_path + run_number_calo + "_" + file_number_calo +'/esums/Type.png')

dfESums = df_floats[df_floats['type'] == 1]
objects = "esums"

#ETEt
fig, axs = plt.subplots(1, 1)
axs.hist(dfESums['ETEt'], bins=400)
axs.set_ylabel('occupancy')
plt.yscale("log")
plt.xlim([1,1048])
axs.set_xlabel('ETEt [hw units]')
plt.title(str(file_calo) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
plt.savefig(plot_path + run_number_calo + "_" + file_number_calo +"/" + objects + '/ETEt.png')

#HTEt
fig, axs = plt.subplots(1, 1)
axs.hist(dfESums['HTEt'], bins=400)
axs.set_ylabel('occupancy')
plt.yscale("log")
plt.xlim([1,1048])
axs.set_xlabel('HTEt [hw units]')
plt.title(str(file_calo) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
plt.savefig(plot_path + run_number_calo + "_" + file_number_calo +"/" + objects + '/HTEt.png')


#ETmissEt
fig, axs = plt.subplots(1, 1)
axs.hist(dfESums['ETmissEt'], bins=400)
axs.set_ylabel('occupancy')
plt.yscale("log")
plt.xlim([1,1048])
axs.set_xlabel('ETmissEt [hw units]')
plt.title(str(file_calo) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
plt.savefig(plot_path + run_number_calo + "_" + file_number_calo +"/" + objects + '/ETmissEt.png')

#HTmissEt
fig, axs = plt.subplots(1, 1)
axs.hist(dfESums['HTmissEt'], bins=400)
axs.set_ylabel('occupancy')
plt.yscale("log")
plt.xlim([1,1048])
axs.set_xlabel('HTmissEt [hw units]')
plt.title(str(file_calo) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
plt.savefig(plot_path + run_number_calo + "_" + file_number_calo +"/" + objects + '/HTmissEt.png')

#ETmissPhi
fig, axs = plt.subplots(1, 1)
axs.hist(dfESums['ETmissPhi'], bins=100)
axs.set_ylabel('occupancy')
plt.xlim([1,200])
axs.set_xlabel('ETmissPhi [hw units]')
plt.title(str(file_calo) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
plt.savefig(plot_path + run_number_calo + "_" + file_number_calo +"/" + objects + '/ETmissPhi.png')

#HTmissPhi
fig, axs = plt.subplots(1, 1)
axs.hist(dfESums['HTmissPhi'], bins=100)
axs.set_ylabel('occupancy')
plt.xlim([1,200])
axs.set_xlabel('HTmissPhi [hw units]')
plt.title(str(file_calo) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
plt.savefig(plot_path + run_number_calo + "_" + file_number_calo +"/" + objects + '/HTmissPhi.png')

print('orbits calo')
print(dfESums['orbit'].min())
print(dfESums['orbit'].max())

for type in range(0,3):

 if type == 0:
  objects="jets"
 elif type == 1:
  objects="egammas"
 elif type ==2:
  objects="taus"
 else:
   print("match type error")

 if objects == "jets":
  dfJets = df[df['type'] == 0]
 elif objects == "egammas":
  dfJets = df[df['type'] == 1]
 elif objects == "taus":
  dfJets = df[df['type'] == 3]
 else:
  print("error which objects?")

 print(objects + " of size " + str(dfJets.size))

 #ET
 fig, axs = plt.subplots(1, 1)
 axs.hist(dfJets['ET'], bins=100)
 axs.set_ylabel('occupancy')
 plt.yscale("log")
 axs.set_xlabel('ET [hw units]')
 plt.title(str(file_calo) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
 plt.savefig(plot_path + run_number_calo + "_" + file_number_calo + "/" + objects + '/ET.png')

#orbit
 fig, axs = plt.subplots(1, 1)
 axs.hist(dfJets['orbit'], bins=2000)
 axs.set_ylabel('occupancy')
 axs.set_xlabel('orbit [hw units]')
 plt.title(str(file_calo) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
 plt.savefig(plot_path + run_number_calo + "_" + file_number_calo +"/" + objects + '/orbit.png')
 
 #bx number
 fig, axs = plt.subplots(1, 1)
# axs.set_ylim(0, max(dfJets['bx']))
 axs.set_xlim(0, 4000)
 axs.hist(dfJets['bx'], bins='auto')
 axs.set_ylabel('occupancy')
 axs.set_xlabel('bx number [hw units]')
 plt.title(str(file_calo) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
 plt.savefig('plots/plots_' + run_number_calo + "_" + file_number_calo +"/" + objects + '/bx.png')
 
 #type
 fig, axs = plt.subplots(1, 1)
 axs.hist(dfJets['type'], bins=4)
 axs.set_ylabel('occupancy')
 axs.set_xlabel('type')
 plt.title(str(file_calo) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
 plt.savefig('plots/plots_' + run_number_calo + "_" + file_number_calo +"/" + objects + '/type.png')
 
 
 #phi
 fig, axs = plt.subplots(1, 1)
 axs.hist(dfJets['phi'], bins=100)
 axs.set_ylabel('occupancy')
 axs.set_xlabel('phi [hw units]')
 plt.title(str(file_calo) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
 plt.savefig('plots/plots_' + run_number_calo + "_" + file_number_calo + "/" + objects + '/phi.png')
 
 #eta
 fig, axs = plt.subplots(1, 1)
 axs.hist(dfJets['eta'], bins=230)
 axs.set_ylabel('occupancy')
 axs.set_xlabel('eta [hw units]')
 plt.title(str(file_calo) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
 plt.savefig('plots/plots_' + run_number_calo + "_" + file_number_calo + "/" + objects + '/eta.png')
 
 
 #iso
 fig, axs = plt.subplots(1, 1)
 axs.hist(dfJets['iso'], bins=200)
 axs.set_ylabel('occupancy')
 axs.set_xlabel('iso [hw units]')
 plt.title(str(file_calo) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
 plt.xlim([0,4])
 plt.savefig('plots/plots_' + run_number_calo + "_" + file_number_calo + "/" + objects + '/iso.png')
 
 #scatter
 fig, axs = plt.subplots(1, 1)
 h = plt.hist2d(dfJets['eta'],dfJets['phi'], bins=101, cmap=plt.cm.jet, norm=matplotlib.colors.LogNorm())
 axs.hist2d(dfJets['eta'],dfJets['phi'], bins=144, cmap=plt.cm.jet, norm=matplotlib.colors.LogNorm())
 axs.set_ylabel('phi [hardware units]')
 axs.set_xlabel('eta [hardware units]')
 axs.set(xlim=(-50.5, 50.5), ylim=(-0.5, 143.5))
 plt.colorbar(h[3])
 plt.title(str(file_calo) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
 plt.savefig('plots/plots_' + run_number_calo + "_" + file_number_calo + "/" + objects + '/scatter_hwunits.png')
 
 
 
 if objects == "jets":
  dfJets = df_floats[df_floats['type'] == 0]
 elif objects == "egammas":
  dfJets = df_floats[df_floats['type'] == 1]
 elif objects == "taus":
  dfJets = df_floats[df_floats['type'] == 3]
 else:
  dfJets = df_floats
 
 #scatter
 fig, axs = plt.subplots(1, 1)
 h = plt.hist2d(dfJets['eta'],dfJets['phi'], bins=40, cmap=plt.cm.jet)
 axs.hist2d(dfJets['eta'],dfJets['phi'], bins=40, cmap=plt.cm.jet)
 axs.set_ylabel('phi [rad]')
 axs.set_xlabel('eta [physical units]')
 plt.colorbar(h[3])
 plt.title(str(file_calo) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
 plt.savefig('plots/plots_' + run_number_calo + "_" + file_number_calo + "/" + objects + '/scatter_float.png')
 
 #scatter: eta,pT
 fig, axs = plt.subplots(1, 1)
 h = plt.hist2d(dfJets['ET'],dfJets['eta'], bins=120, cmap=plt.cm.jet)
 axs.hist2d(dfJets['ET'],dfJets['eta'], bins=120, cmap=plt.cm.jet)
 axs.set_ylabel('Eta [physical units]')
 axs.set_xlabel('ET [GeV]')
 plt.colorbar(h[3])
 plt.title(str(file_calo) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
 plt.savefig('plots/plots_' + run_number_calo + "_" + file_number_calo + "/" + objects + '/scatter_ET_Eta_float.png')
 
 #scatter: eta,phi
 fig, axs = plt.subplots(1, 1)
 h = plt.hist2d(dfJets['eta'],dfJets['phi'], bins=80, cmap=plt.cm.jet, norm=matplotlib.colors.LogNorm())
 axs.hist2d(dfJets['eta'],dfJets['phi'], bins=80, cmap=plt.cm.jet, norm=matplotlib.colors.LogNorm())
 axs.set(xlim=(-2.5, 2.5), ylim=(0, 6.282))
 axs.set_ylabel('phi [rad]')
 axs.set_xlabel('eta [physical units]')
 plt.colorbar(h[3])
 plt.title(str(file_calo) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
 plt.savefig('plots/plots_' + run_number_calo + "_" + file_number_calo + "/" + objects + '/scatter_Eta_Phi_float_log.png')
 
 
 #scatter: ET, phi
 fig, axs = plt.subplots(1, 1)
 h = plt.hist2d(dfJets['ET'],dfJets['phi'], bins=20, cmap=plt.cm.jet, norm=matplotlib.colors.LogNorm())
 axs.hist2d(dfJets['ET'],dfJets['phi'], bins=20, cmap=plt.cm.jet, norm=matplotlib.colors.LogNorm())
 axs.set_ylabel('phi [rad]')
 axs.set_xlabel('ET [GeV]')
 plt.colorbar(h[3])
 plt.title(str(file_calo) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
 plt.savefig('plots/plots_' + run_number_calo + "_" + file_number_calo + "/" + objects + '/scatter_ET_Phi_float.png')
 
 #et
 fig, axs = plt.subplots(1, 1)
 axs.hist(dfJets['ET'], bins=100, color='r')
 axs.set_ylabel('occupancy')
 plt.yscale("log")
 axs.set_xlabel('et [GeV]')
 plt.title(str(file_calo) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
 plt.savefig('plots/plots_' + run_number_calo + "_" + file_number_calo + "/"+ objects + '/et_float.png')
 
 #phi
 fig, axs = plt.subplots(1, 1)
 axs.hist(dfJets['phi'], bins=100, color='r')
 axs.set_ylabel('occupancy')
 axs.set_xlabel('phi [rad]')
 plt.title(str(file_calo) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
 plt.savefig('plots/plots_' + run_number_calo + "_" + file_number_calo + "/"+ objects + '/phi_float.png')
 
 #eta
 fig, axs = plt.subplots(1, 1)
 axs.hist(dfJets['eta'], bins=100, color='r')
 axs.set_ylabel('occupancy')
 axs.set_xlabel('eta [physical units]')
 plt.title(str(file_calo) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
 plt.savefig('plots/plots_' + run_number_calo + "_" + file_number_calo + "/"+ objects + '/eta_float.png')

df_floats.to_parquet('/home/scouter/scunpack/unpacked/scout_CALO_' + run_number_calo + '_' + file_number_calo + '.pqt')

### MUON PLOTS ###
adictionary_gmt=scunpack.convert(dir + file_gmt, 0, nEventsMuon, wantDict=True,wantFloats=False,excludeIntermediate=excludeIntermediate_,excludeFinal=excludeFinal_)

df_gmt = pd.DataFrame.from_dict(adictionary_gmt)
del adictionary_gmt

duplicate_bx = df_gmt.pivot_table(columns=['bx'], aggfunc='size')
print(duplicate_bx.shape)
print(duplicate_bx.shape)
print(df_gmt.shape)
print((~df_gmt.duplicated(subset=['bx'])).sum())

#pt
fig, axs = plt.subplots(1, 1)
axs.hist(df_gmt['pt'], bins=200)
axs.set_ylabel('occupancy')
plt.yscale("log")
axs.set_xlabel('pt [hw units]')
plt.title(str(time))
plt.title(str(file_gmt) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/plots_' + run_number_gmt + "_" + file_number_gmt  + '/muons/pt.png')

#pt
fig, axs = plt.subplots(1, 1)
axs.set_xlim(0, 50)
axs.hist(df_gmt['pt'], bins=512)
axs.set_ylabel('occupancy')
axs.set_xlabel('pt [hw units]')
plt.title(str(time))
plt.title(str(file_gmt) + "       " + str(time), fontsize = 8)
plt.savefig('plots/plots_' + run_number_gmt + "_" + file_number_gmt  + '/muons/pt_unlogged.png')

#orbit
fig, axs = plt.subplots(1, 1)
axs.hist(df_gmt['orbit'], bins=2000)
axs.set_ylabel('occupancy')
axs.set_xlabel('orbit [hw units]')
plt.title(str(time))
plt.title(str(file_gmt) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/plots_' + run_number_gmt + "_" + file_number_gmt  + '/muons/orbit.png')

print('orbits')
print(df_gmt['orbit'].min())
print(df_gmt['orbit'].max())
df_gmt_int_orbits = df_gmt['orbit']

fig, axs = plt.subplots(1, 1)
axs.set_ylim(0, max(df_gmt['bx']))
axs.hist(df_gmt['bx'], bins='auto')
axs.set_ylabel('occupancy')
axs.set_xlabel('bx number [hw units]')
plt.title(str(time))
plt.title(str(file_gmt) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/plots_' + run_number_gmt + "_" + file_number_gmt  + '/muons/bx.png')

#intermediate marker
fig, axs = plt.subplots(1, 1)
axs.hist(df_gmt['interm'], bins=4)
axs.set_ylabel('occupancy')
axs.set_xlabel('intermediate marker [hw units]')
plt.title(str(time))
plt.title(str(file_gmt) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/plots_' + run_number_gmt + "_" + file_number_gmt  + '/muons/interm.png')

#phi
fig, axs = plt.subplots(1, 1)
axs.hist(df_gmt['phi'], bins=200)
axs.set_ylabel('occupancy')
axs.set_xlabel('phi [hw units]')
plt.title(str(time))
plt.title(str(file_gmt) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/plots_' + run_number_gmt + "_" + file_number_gmt + '/muons/phi.png')

#eta
fig, axs = plt.subplots(1, 1)
axs.hist(df_gmt['eta'], bins=200)
axs.set_ylabel('occupancy')
axs.set_xlabel('eta [hw units]')
plt.title(str(time))
plt.title(str(file_gmt) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/plots_' + run_number_gmt + "_" + file_number_gmt + '/muons/eta.png')

#charge
fig, axs = plt.subplots(1, 1)
axs.hist(df_gmt['charge'], bins=2)
axs.set_ylabel('occupancy')
axs.set_xlabel('charge [hw units]')
plt.title(str(time))
plt.title(str(file_gmt) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/plots_' + run_number_gmt + "_" + file_number_gmt + '/muons/charge.png')

#iso
fig, axs = plt.subplots(1, 1)
axs.hist(df_gmt['iso'], bins=10)
axs.set_ylabel('occupancy')
axs.set_xlabel('iso [hw units]')
plt.title(str(time))
plt.title(str(file_gmt) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/plots_' + run_number_gmt + "_" + file_number_gmt + '/muons/iso.png')

#index
fig, axs = plt.subplots(1, 1)
axs.hist(df_gmt['index'], bins=200)
axs.set_ylabel('occupancy')
axs.set_xlabel('index [hw units]')
plt.title(str(time))
plt.title(str(file_gmt) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/plots_' + run_number_gmt + "_" + file_number_gmt + '/muons/index.png')

#qual
fig, axs = plt.subplots(1, 1)
axs.hist(df_gmt['qual'], bins=20)
axs.set_ylabel('occupancy')
axs.set_xlabel('Quality [hw units]')
plt.title(str(time))
plt.title(str(file_gmt) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/plots_' + run_number_gmt + "_" + file_number_gmt + '/muons/qual.png')

#phie
fig, axs = plt.subplots(1, 1)
axs.hist(df_gmt['phie'], bins=200)
axs.set_ylabel('occupancy')
axs.set_xlabel('Extrapolated Phi [hw units]')
plt.title(str(time))
plt.title(str(file_gmt) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/plots_' + run_number_gmt + "_" + file_number_gmt + '/muons/phie.png')


#etae
fig, axs = plt.subplots(1, 1)
axs.hist(df_gmt['etae'], bins=200)
axs.set_ylabel('occupancy')
axs.set_xlabel('Extrapolated Eta [hw units]')
plt.title(str(time))
plt.title(str(file_gmt) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/plots_' + run_number_gmt + "_" + file_number_gmt + '/muons/etae.png')

#unconstrained pt
fig, axs = plt.subplots(1, 1)
axs.hist(df_gmt['ptunconstrained'], bins=200)
axs.set_ylabel('occupancy')
axs.set_xlabel('Unconstrained pT [hw units]')
plt.yscale("log")
plt.title(str(file_gmt) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/plots_' + run_number_gmt + "_" + file_number_gmt + '/muons/ptunconstrained.png')

#dxy
fig, axs = plt.subplots(1, 1)
axs.hist(df_gmt['dxy'], bins=4)
axs.set_ylabel('occupancy')
axs.set_xlabel('dxy [hw units]')
plt.title(str(time))
plt.title(str(file_gmt) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/plots_' + run_number_gmt + "_" + file_number_gmt + '/muons/dxy.png')

#df_gmt.to_parquet('/home/scouter/scunpack/unpacked/scout_GMT_' + run_number_gmt + '_' + file_number_gmt + '.pqt')
adictionary_gmt=scunpack.convert(dir + file_gmt, 0, nEventsMuon, wantDict=True,wantFloats=True,excludeIntermediate=excludeIntermediate_,excludeFinal=excludeFinal_)
df_gmt = pd.DataFrame.from_dict(adictionary_gmt)
del adictionary_gmt
df_gmt.drop('orbit', axis=1, inplace=True)
df_gmt["orbit"]=df_gmt_int_orbits

df_gmt.to_parquet('/home/scouter/scunpack/unpacked/scout_GMT_' + run_number_gmt + '_' + file_number_gmt + '.pqt')

#pt
fig, axs = plt.subplots(1, 1)
axs.hist(df_gmt['pt'], bins=200, color='r')
axs.set_ylabel('occupancy')
plt.yscale("log")
axs.set_xlabel('pt [GeV]')
plt.title(str(time))
plt.title(str(file_gmt) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/plots_' + run_number_gmt + "_" + file_number_gmt + '/muons/pt_float.png')

#phi
fig, axs = plt.subplots(1, 1)
axs.hist(df_gmt['phi'], bins=200, color='r')
axs.set_ylabel('occupancy')
axs.set_xlabel('phi [rad]')
plt.title(str(time))
plt.title(str(file_gmt) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/plots_' + run_number_gmt + "_" + file_number_gmt + '/muons/phi_float.png')

#phie
fig, axs = plt.subplots(1, 1)
axs.hist(df_gmt['phie'], bins=200, color='r')
axs.set_ylabel('occupancy')
axs.set_xlabel('Extrapolated Phi rad]')
plt.title(str(time))
plt.title(str(file_gmt) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/plots_' + run_number_gmt + "_" + file_number_gmt + '/muons/phie_float.png')

#eta
fig, axs = plt.subplots(1, 1)
axs.hist(df_gmt['eta'], bins=200, color='r')
axs.set_ylabel('occupancy')
axs.set_xlabel('eta [physical units]')
plt.title(str(time))
plt.title(str(file_gmt) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/plots_' + run_number_gmt + "_" + file_number_gmt + '/muons/eta_float.png')

#charge
fig, axs = plt.subplots(1, 1)
axs.hist(df_gmt['charge'], bins=2, color='r')
axs.set_ylabel('occupancy')
axs.set_xlabel('charge [electric charge]')
plt.title(str(time))
plt.title(str(file_gmt) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/plots_' + run_number_gmt + "_" + file_number_gmt + '/muons/charge_float.png')

#etae
fig, axs = plt.subplots(1, 1)
axs.hist(df_gmt['etae'], bins=200, color='r')
axs.set_ylabel('occupancy')
axs.set_xlabel('Extrapolated Eta [physical units]')
plt.title(str(time))
plt.title(str(file_gmt) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/plots_' + run_number_gmt + "_" + file_number_gmt + '/muons/etae_float.png')


#actually select egammas

df_gmt.drop(columns=['charge','iso','ptunconstrained','dxy','index','qual','iso','interm','bytes'], axis=1, inplace=True)
dfJets.drop(columns=['ET','iso','ETEt','HTEt','ETmissEt','HTmissEt','ETmissPhi','HTmissPhi'], axis=1, inplace=True)

df_gmt.bx.astype('int32')
dfJets.bx.astype('int32')
df_gmt.orbit.astype('int64')
dfJets.orbit.astype('int64')
df_gmt.phi.astype('float32')
dfJets.phi.astype('float32')
df_gmt.eta.astype('float32')
dfJets.eta.astype('float32')



print(df_gmt.columns.tolist())
print(dfJets.columns.tolist())

#dfJets = df_floats[df_floats['type'] == 0].sort_values(by=['orbit'])

dfJets = dfJets[dfJets['type'] == 1]

#df_gmt.sort_values(by=['orbit'])
dfJets.drop(columns=['type'])
df_intersect=pd.merge(df_gmt, dfJets, how='inner', on=['orbit'])

del df_gmt
del dfJets

df_intersect.dropna(inplace=True)
#print(df_intersect.size())

print('orbits')
#print(df_gmt['orbit'])
print(df_intersect['orbit'].min())
print(df_intersect['orbit'].max())

#df_intersect.to_parquet('/home/scouter/scunpack/unpacked/scout_combi_' + run_number_gmt + '_' + file_number_gmt + '.pqt')
#df_intersect['compare'] = (abs(df_intersect['bx_x'] - df_intersect['bx_y']) < 10 )
#df_intersect = df_intersect.drop(df_intersect['compare'] == False)

#print(df_intersect.size())

df_intersect['deltaPhi'] = df_intersect['phi_x'] - df_intersect['phi_y'] + math.pi

df_intersect['deltaEta'] = df_intersect['eta_x'] - df_intersect['eta_y']

df_intersect['deltaPhi'] = df_intersect['deltaPhi'].apply(lambda x: x if x > 0 else x + 2*math.pi) 

#etae
fig, axs = plt.subplots(1, 1)
axs.hist(df_intersect["deltaPhi"], bins=100, color='r')
axs.set_ylabel('occupancy')
axs.set_xlabel('delta Phi')
plt.title(str(time))
plt.title(str(file_gmt) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/plots_' + run_number_gmt + "_" + file_number_gmt + '/muons/egmma_muon_deltaphi.png')

#np.savetxt('/home/scouter/scunpack/unpacked/mu_egamma.csv', np.c_[df_intersect['bx_x'], df_intersect['bx_y'], df_intersect['orbit'], df_intersect['phi_x'], df_intersect['phi_y'], df_intersect['deltaPhi'], df_intersect['pt'], df_intersect['deltaEta']], delimiter=',', newline='\r\n', header='bx, bx_jet, orbit, phi, phi_jet, deltaPhi, pt_mu, deltaEta')

#np.savetxt('mu.csv', np.c_[df_gmt['bx'], df_gmt['orbit'], df_gmt['phi']], delimiter=',', newline='\r\n', header='bx, orbit, phi')
#np.savetxt('mu_' + run_number_gmt +'.csv', np.c_[df_gmt['pt'], df_gmt['eta'], df_gmt['index']], delimiter=',', newline='\r\n', header='pt, eta, index')

#np.savetxt('egamma.csv', np.c_[dfJets['bx'], dfJets['orbit'], dfJets['phi']], delimiter=',', newline='\r\n', header='bx, orbit, phi')
