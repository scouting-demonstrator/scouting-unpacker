#Reminder of compile command
#c++ -fPIC -std=c++11 -I /home/tjames/tf2_gpu_env/include/python3.7m/ -o skac.so -shared -L /home/tjames/tf2_gpu_env/lib -lpython3.7m skimAndConvert.cc
#I had to copy libpython3.7m.so.1.0 locally
#conda activate tf2_22_gpu

import sys
file = sys.argv[1]
nevents = int(sys.argv[2])
objects = str(sys.argv[3])

floats = False
excludeIntermediate_=True
excludeFinal_=False
dir = sys.argv[4]
import skac
adictionary=skac.convertcalo(dir + file, nevents, wantDict=True, wantFloats=False)
print("convert done")
import json
f=open(file + '.json','w')
json.dump(adictionary,f)
f.close()
print("dumped to json")
import pandas as pd
df = pd.read_json(file + '.json')
print("read json file")
df.head()
#df['brillword'].to_csv('afile.csv')
import datetime
import matplotlib.pyplot as plt
from matplotlib import colors, rcParams
from matplotlib.ticker import PercentFormatter
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.backends.backend_agg

print(objects)
if objects == 'jets':
 dfJets = df[df['type'] == 0]
elif objects == "egammas":
 dfJets = df[df['type'] == 1]
elif objects == "taus":
 dfJets = df[df['type'] == 3]
else:
 dfJets = df

print(dfJets.size)
time = datetime.datetime.now().strftime("%c")

matplotlib.rc('figure', dpi = '300')

#et
fig, axs = plt.subplots(1, 1)
axs.hist(dfJets['ET'], bins=100)
axs.set_ylabel('frequency')
plt.yscale("log")
axs.set_xlabel('ET [hw units]')
plt.title(str(file) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
plt.savefig('plotsCalo/' + objects + '/ET.png')

#orbit
fig, axs = plt.subplots(1, 1)
axs.hist(dfJets['orbit'], bins=1000)
axs.set_ylabel('frequency')
axs.set_xlabel('orbit [hw units]')
plt.title(str(file) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
plt.savefig('plotsCalo/' + objects + '/orbit.png')

#bx number
fig, axs = plt.subplots(1, 1)
axs.hist(dfJets['bx'], bins=1000)
axs.set_ylabel('frequency')
#plt.xlim([3000,4000])
axs.set_xlabel('bx number [hw units]')
plt.title(str(file) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
plt.savefig('plotsCalo/' + objects + '/bx.png')

#type
fig, axs = plt.subplots(1, 1)
axs.hist(dfJets['type'], bins=4)
axs.set_ylabel('frequency')
axs.set_xlabel('type')
plt.title(str(file) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
plt.savefig('plotsCalo/' + objects + '/type.png')


#phi
fig, axs = plt.subplots(1, 1)
axs.hist(dfJets['phi'], bins=100)
axs.set_ylabel('frequency')
axs.set_xlabel('phi [hw units]')
plt.title(str(file) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
plt.savefig('plotsCalo/' + objects + '/phi.png')

#eta
fig, axs = plt.subplots(1, 1)
axs.hist(dfJets['eta'], bins=230)
axs.set_ylabel('frequency')
axs.set_xlabel('eta [hw units]')
plt.xlim([30,40])
plt.title(str(file) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
plt.savefig('plotsCalo/' + objects + '/eta.png')


#iso
fig, axs = plt.subplots(1, 1)
axs.hist(dfJets['iso'], bins=200)
axs.set_ylabel('frequency')
axs.set_xlabel('iso [hw units]')
plt.title(str(file) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
plt.xlim([0,4])
plt.savefig('plotsCalo/' + objects + '/iso.png')

#scatter
fig, axs = plt.subplots(1, 1)
h = plt.hist2d(dfJets['eta'],dfJets['phi'], bins=101, cmap=plt.cm.jet, norm=matplotlib.colors.LogNorm())
axs.hist2d(dfJets['eta'],dfJets['phi'], bins=144, cmap=plt.cm.jet, norm=matplotlib.colors.LogNorm())
axs.set_ylabel('phi [hardware units]')
axs.set_xlabel('eta [hardware units]')
axs.set(xlim=(-50.5, 50.5), ylim=(-0.5, 143.5))
plt.colorbar(h[3])
plt.title(str(file) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
plt.savefig('plotsCalo/' + objects + '/scatter_hwunits.png')

#scatter: eta,pT
fig, axs = plt.subplots(1, 1)
h = plt.hist2d(dfJets['ET'],dfJets['eta'], bins=40, cmap=plt.cm.jet)
axs.hist2d(dfJets['ET'],dfJets['eta'], bins=40, cmap=plt.cm.jet)
axs.set_ylabel('Eta [hardware units]')
axs.set_xlabel('ET [hardware units]')
plt.colorbar(h[3])
plt.title(str(file) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
plt.savefig('plotsCalo/' + objects + '/scatter_ET_Eta_hwunits.png')


adictionary=skac.convertcalo(dir + file, nevents, wantDict=True,wantFloats=True)

f=open(file + '_floats.json','w')
json.dump(adictionary,f)
f.close()
df = pd.read_json(file + '_floats.json')
matplotlib.rc('figure', dpi = '300')


if objects == "jets":
 dfJets = df[df['type'] == 0]
elif objects == "egammas":
 dfJets = df[df['type'] == 1]
elif objects == "taus":
 dfJets = df[df['type'] == 3]
else:
 dfJets = df

#scatter
fig, axs = plt.subplots(1, 1)
h = plt.hist2d(dfJets['eta'],dfJets['phi'], bins=40, cmap=plt.cm.jet)
axs.hist2d(dfJets['eta'],dfJets['phi'], bins=40, cmap=plt.cm.jet)
axs.set_ylabel('phi [rad]')
axs.set_xlabel('eta [physical units]')
plt.colorbar(h[3])
plt.title(str(file) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
plt.savefig('plotsCalo/' + objects + '/scatter_float.png')

#scatter: eta,pT
fig, axs = plt.subplots(1, 1)
h = plt.hist2d(dfJets['ET'],dfJets['eta'], bins=40, cmap=plt.cm.jet)
axs.hist2d(dfJets['ET'],dfJets['eta'], bins=40, cmap=plt.cm.jet)
axs.set_ylabel('Eta [physical units]')
axs.set_xlabel('ET [GeV]')
plt.colorbar(h[3])
plt.title(str(file) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
plt.savefig('plotsCalo/' + objects + '/scatter_ET_Eta_float.png')

#scatter: eta,phi
fig, axs = plt.subplots(1, 1)
h = plt.hist2d(dfJets['eta'],dfJets['phi'], bins=80, cmap=plt.cm.jet, norm=matplotlib.colors.LogNorm())
axs.hist2d(dfJets['eta'],dfJets['phi'], bins=80, cmap=plt.cm.jet, norm=matplotlib.colors.LogNorm())
axs.set(xlim=(-2.5, 2.5), ylim=(0, 6.282))
axs.set_ylabel('phi [rad]')
axs.set_xlabel('eta [physical units]')
plt.colorbar(h[3])
plt.title(str(file) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
plt.savefig('plotsCalo/' + objects + '/scatter_Eta_Phi_float_log.png')


#scatter: ET, phi
fig, axs = plt.subplots(1, 1)
h = plt.hist2d(dfJets['ET'],dfJets['phi'], bins=20, cmap=plt.cm.jet, norm=matplotlib.colors.LogNorm())
axs.hist2d(dfJets['ET'],dfJets['phi'], bins=20, cmap=plt.cm.jet, norm=matplotlib.colors.LogNorm())
axs.set_ylabel('phi [rad]')
axs.set_xlabel('ET [GeV]')
plt.colorbar(h[3])
plt.title(str(file) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
plt.savefig('plotsCalo/' + objects + '/scatter_ET_Phi_float.png')


dfET = df[df['ET'] > 800]
#et
fig, axs = plt.subplots(1, 1)
axs.plot(dfET['eta'], color='r')
axs.set_ylabel('eta')
#plt.yscale("log")
axs.set_xlabel('time')
plt.title(str(file) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
plt.savefig('plotsCalo/' + objects + '/eta_float_vtime.png')


#et
fig, axs = plt.subplots(1, 1)
axs.hist(dfJets['ET'], bins=100, color='r')
axs.set_ylabel('frequency')
plt.yscale("log")
axs.set_xlabel('et [GeV]')
plt.title(str(file) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
plt.savefig('plotsCalo/' + objects + '/et_float.png')


#phi
fig, axs = plt.subplots(1, 1)
axs.hist(dfJets['phi'], bins=100, color='r')
axs.set_ylabel('frequency')
axs.set_xlabel('phi [rad]')
plt.title(str(file) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
plt.savefig('plotsCalo/' + objects + '/phi_float.png')

#eta
fig, axs = plt.subplots(1, 1)
axs.hist(dfJets['eta'], bins=100, color='r')
axs.set_ylabel('frequency')
axs.set_xlabel('eta [physical units]')
plt.title(str(file) + " , plot generated at: " + str(time) + ", "  + str(objects), fontsize = 7)
plt.savefig('plotsCalo/' + objects + '/eta_float.png')

