#Reminder of compile command
#c++ -fPIC -std=c++11 -I /home/tjames/tf2_gpu_env/include/python3.7m/ -o skac.so -shared -L /home/tjames/tf2_gpu_env/lib -lpython3.7m skimAndConvert.cc
#I had to copy libpython3.7m.so.1.0 locally
#conda activate tf2_22_gpu

import sys
file = sys.argv[1]
nevents = int(sys.argv[2])
floats = False
excludeIntermediate_=True
excludeFinal_=False
dir = "/home/tjames/ugmt-data-formatter/ugmt-data-formatter/python/in_progress/"
import skac
adictionary=skac.convert(dir + file, nevents, wantDict=True,wantFloats=floats,excludeIntermediate=excludeIntermediate_,excludeFinal=excludeFinal_)
print("convert done")
import json
f=open(file + '.json','w')
json.dump(adictionary,f)
f.close()
import pandas as pd
df = pd.read_json(file + '.json')
df.head()
df['bx'].to_csv('bx.csv')
import datetime
import matplotlib.pyplot as plt
from matplotlib import colors, rcParams
from matplotlib.ticker import PercentFormatter
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.backends.backend_agg
matplotlib.rc('figure', dpi = '300')

time = datetime.datetime.now().strftime("%c")
#pt
fig, axs = plt.subplots(1, 1)
axs.hist(df['pt'], bins=200)
axs.set_ylabel('frequency')
plt.yscale("log")
axs.set_xlabel('pt [hw units]')
plt.title(str(time))
plt.title(str(file) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/pt.png')

#orbit
fig, axs = plt.subplots(1, 1)
axs.hist(df['orbit'], bins=500)
axs.set_ylabel('frequency')
axs.set_xlabel('orbit [hw units]')
plt.title(str(time))
plt.title(str(file) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/orbit.png')

#bx number
fig, axs = plt.subplots(1, 1)
axs.hist(df['bx'], bins=4000)
axs.set_ylabel('frequency')
axs.set_xlabel('bx number [hw units]')
plt.title(str(time))
plt.title(str(file) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/bx.png')

#intermediate marker
fig, axs = plt.subplots(1, 1)
axs.hist(df['interm'], bins=4)
axs.set_ylabel('frequency')
axs.set_xlabel('intermediate marker [hw units]')
plt.title(str(time))
plt.title(str(file) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/interm.png')

#phi
fig, axs = plt.subplots(1, 1)
axs.hist(df['phi'], bins=200)
axs.set_ylabel('frequency')
axs.set_xlabel('phi [hw units]')
plt.title(str(time))
plt.title(str(file) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/phi.png')

#eta
fig, axs = plt.subplots(1, 1)
axs.hist(df['eta'], bins=200)
axs.set_ylabel('frequency')
axs.set_xlabel('eta [hw units]')
plt.title(str(time))
plt.title(str(file) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/eta.png')

#charge
fig, axs = plt.subplots(1, 1)
axs.hist(df['charge'], bins=2)
axs.set_ylabel('frequency')
axs.set_xlabel('charge [hw units]')
plt.title(str(time))
plt.title(str(file) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/charge.png')

#iso
fig, axs = plt.subplots(1, 1)
axs.hist(df['iso'], bins=10)
axs.set_ylabel('frequency')
axs.set_xlabel('iso [hw units]')
plt.title(str(time))
plt.title(str(file) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/iso.png')

#index
fig, axs = plt.subplots(1, 1)
axs.hist(df['index'], bins=200)
axs.set_ylabel('frequency')
axs.set_xlabel('index [hw units]')
plt.title(str(time))
plt.title(str(file) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/index.png')

#qual
fig, axs = plt.subplots(1, 1)
axs.hist(df['qual'], bins=20)
axs.set_ylabel('frequency')
axs.set_xlabel('Quality [hw units]')
plt.title(str(time))
plt.title(str(file) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/qual.png')

#phie
fig, axs = plt.subplots(1, 1)
axs.hist(df['phie'], bins=200)
axs.set_ylabel('frequency')
axs.set_xlabel('Extrapolated Phi [hw units]')
plt.title(str(time))
plt.title(str(file) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/phie.png')

#etae
fig, axs = plt.subplots(1, 1)
axs.hist(df['etae'], bins=200)
axs.set_ylabel('frequency')
axs.set_xlabel('Extrapolated Eta [hw units]')
plt.title(str(time))
plt.title(str(file) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/etae.png')

#unconstrained pt
fig, axs = plt.subplots(1, 1)
axs.hist(df['ptunconstrained'], bins=200)
axs.set_ylabel('frequency')
axs.set_xlabel('Unconstrained pT [hw units]')
plt.yscale("log")
plt.title(str(file) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/ptunconstrained.png')

#dxy
fig, axs = plt.subplots(1, 1)
axs.hist(df['dxy'], bins=4)
axs.set_ylabel('frequency')
axs.set_xlabel('dxy [hw units]')
plt.title(str(time))
plt.title(str(file) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/dxy.png')

adictionary=skac.convert(dir + file, nevents, wantDict=True,wantFloats=True,excludeIntermediate=excludeIntermediate_,excludeFinal=excludeFinal_)

f=open(file + '_floats.json','w')
json.dump(adictionary,f)
f.close()
df = pd.read_json(file + '_floats.json')
matplotlib.rc('figure', dpi = '300')

#pt
fig, axs = plt.subplots(1, 1)
axs.hist(df['pt'], bins=200, color='r')
axs.set_ylabel('frequency')
plt.yscale("log")
axs.set_xlabel('pt [GeV]')
plt.title(str(time))
plt.title(str(file) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/pt_float.png')

#phi
fig, axs = plt.subplots(1, 1)
axs.hist(df['phi'], bins=200, color='r')
axs.set_ylabel('frequency')
axs.set_xlabel('phi [rad]')
plt.title(str(time))
plt.title(str(file) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/phi_float.png')

#eta
fig, axs = plt.subplots(1, 1)
axs.hist(df['eta'], bins=200, color='r')
axs.set_ylabel('frequency')
axs.set_xlabel('eta [physical units]')
plt.title(str(time))
plt.title(str(file) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/eta_float.png')

#charge
fig, axs = plt.subplots(1, 1)
axs.hist(df['charge'], bins=2, color='r')
axs.set_ylabel('frequency')
axs.set_xlabel('charge [electric charge]')
plt.title(str(time))
plt.title(str(file) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/charge_float.png')

#phie
fig, axs = plt.subplots(1, 1)
axs.hist(df['phie'], bins=200, color='r')
axs.set_ylabel('frequency')
axs.set_xlabel('Extrapolated Phi rad]')
plt.title(str(time))
plt.title(str(file) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/phie_float.png')

#etae
fig, axs = plt.subplots(1, 1)
axs.hist(df['etae'], bins=200, color='r')
axs.set_ylabel('frequency')
axs.set_xlabel('Extrapolated Eta [physical units]')
plt.title(str(time))
plt.title(str(file) + "       " + str(time) + "      " + str(nevents) + " events", fontsize = 8)
plt.savefig('plots/etae_float.png')

dfTest = df

#scatter: eta,pT
fig, axs = plt.subplots(1, 1)
h = plt.hist2d(dfTest['eta'],dfTest['etae'], bins=100, cmap=plt.cm.jet)
axs.hist2d(dfTest['eta'],dfTest['etae'], bins=100, cmap=plt.cm.jet)
axs.set_ylabel('etae [physical units]')
axs.set_xlabel('eta [physical units]')
plt.colorbar(h[3])
plt.title(str(file) + " , plot generated at: " + str(time) + ", "  + "muons", fontsize = 7)
plt.savefig('plots/scatter_eta_etae_float.png')


