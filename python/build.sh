#!/bin/bash
# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
# -DPARQUET_INCLUDE_DIR=/usr/include/arrow  -DPARQUET_LIBRARIES -DPARQUET_LIBRARY_DIRS=/usr/lib/x86_64-linux-gnu


set -ex
export BUILD_DIR=skac
mkdir -p $BUILD_DIR
pushd $BUILD_DIR
cmake  ../
make 
popd
