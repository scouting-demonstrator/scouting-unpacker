file=$1
if [ $# -ne 1 ]; then
    echo incorrect number of arguments found, please input file name as argument e.g ./trim_passthrough.sh scout_GMT_XXXXXX_YYYYYY.dat.bz2
    exit 1
fi

echo trimming "$file" ..
loc=$(bunzip2 -cd "$file" | hexdump -ve '"%08.8_ax  \"' -e '8/4 "%08x \" "\n\"' | head -n500000 | grep deadbeef | tail -n1 | cut -c1-8)
locdec=$((0x$loc + 0x20))
prefix=$(echo "$file" | cut -d "." -f 1)
echo taking first $locdec bytes, ending on a deadbeef
bunzip2 -cd "$file" | head -c$locdec >& "$prefix\_trimmed.dat"
hexdump -ve '"%08.8_ax  \"' -e '8/4 "%08x \" "\n\"' $prefix\_trimmed.dat >& "$prefix\_trimmed.hex"
echo done, please enjoy $prefix\_trimmed.dat and $prefix\_trimmed.hex
