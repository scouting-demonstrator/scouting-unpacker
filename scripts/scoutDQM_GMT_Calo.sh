#!/bin/bash
file_path_base=$1
nevents=10000000
run=3
a=0
while (a=0)
do
    file_calo=`ls -Art $file_path_base | grep dat | grep CALO | grep $run | grep -v json | tail -n 1`
    if [ -z "$file_calo" ]
    then 
    	echo "no calo file found"
	sleep 30s
	break
    fi
    echo $file_calo
    FILESIZE=$(stat -c%s "$file_path_base/$file_calo")
    echo $FILESIZE
    if grep -q "bz2" <<< $file_calo; then
    	bzip2 -dkvv "$file_path_base/$file_calo"
    fi
    unzippedFileCALO=${file_calo%.bz2}
    
    file_gmt=`ls -Art $file_path_base | grep dat | grep GMT | grep $run | grep -v json | tail -n 1`
    if [ -z "$file_gmt" ]
    then 
    	echo "no gmt file found"
	sleep 30s
	break
    fi
    echo $file_gmt
    if grep -q "bz2" <<< $file_calo; then
    	bzip2 -dkvv "$file_path_base/$file_gmt"
    fi
    unzippedFileGMT=${file_gmt%.bz2}
    python3 ../python/skac_gmt_calo.py "$unzippedFileGMT" "$unzippedFileCALO" $nevents "$file_path_base/"
    cd $file_path_base/..
    
    mv "$file_path_base/$file_gmt" "$file_path_base/unpacked"
    mv "$file_path_base/$file_calo" "$file_path_base/unpacked"
    mv "$file_path_base/$unzippedFileGMT" "$file_path_base/unpacked"
    #mv "$file_path_base/$unzippedFileCALO" "$file_path_base/unpacked"
    
    #unpackedFileGMT=${file_gmt%.json}
    #cd ../unpacked
    #lbzip2 -v --fast "$unpackedFileGMT"
     
    export PYTHONPATH=/opt/scunpack/site-packages:$PYTHONPATH
    sigal build -f
    cd -
    a=1
    exit
    sleep 30s
done
