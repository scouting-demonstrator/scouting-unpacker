#!/bin/bash -e
BUILD_ARCH=x86_64
SCRIPTDIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
RHEL_MAJOR=$(cat /etc/redhat-release | cut -d' ' -f4 | cut -d'.' -f1)

echo "Checking if necessary build packages are installed..."
if [ "$RHEL_MAJOR" = "7" ]; then
  if ! rpm -q rpm-build; then
    echo "\nPlease install missing packages."
    exit 1
  fi
else
  if ! rpm -q gcc redhat-rpm-config rpm-build zlib-devel; then
    echo ""
    echo "Please install missing packages."
    exit 1
  fi
fi
echo "..Build dependencies OK"
echo ""

cd $SCRIPTDIR/..
BASEDIR=$PWD
PARAMCACHE="paramcache"
NLINES=1
ASK="1"
RAWVER=$(git describe --tags --long || echo 'v0.0.0') # If git describe fails, we set a default version number.
VER=$(echo ${RAWVER} | sed 's/^v//' | awk '{split($0,a,"-"); print a[1]}')
REL=$(echo "${RAWVER}" | sed 's/^v//' | awk '{split($0,a,"-"); print a[2]}')

echo "Version $VER, release $REL"

if [ -n "$1" ]; then
  if [ "$1" = "--batch" ]; then
    ASK="0"
  fi

  if [ "$1" = "-b" ]; then
    ASK="0"
  fi

  if [ $ASK = "0" ]; then #check $2 if found
    if [ -n "$2" ]; then
      PARAMCACHE=${2##*/}
    fi
  fi
  if [ $ASK = "1" ]; then #take $1
    PARAMCACHE=${1##*/}
  fi
fi

echo "Using cache file $PARAMCACHE"

if [ -f $SCRIPTDIR/$PARAMCACHE ]; then
  readarray lines <$SCRIPTDIR/$PARAMCACHE
  for ((i = 0; i < ${NLINES}; i++)); do
    lines[$i]=$(echo -n ${lines[$i]} | tr -d "\n")
  done
else
  for ((i = 0; i < ${NLINES}; i++)); do
    lines[$i]=""
  done
fi

if [ $ASK = "1" ]; then

  echo "This is the scunpack build script. It will now ask for several configuration parameters."
  echo "Use -b cmdline parameter to build from cache without waiting for input"
  echo "   ... press any key to continue ..."
  read readin

  echo "Dummy parameter, this will be used to modify/obtain build parameters from cache (press enter for \"${lines[0]}\"):"
  readin=""
  read readin
  if [ ${#readin} != "0" ]; then
    lines[0]=$readin
  fi

fi #ask

#update cache file
if [ -f $SCRIPTDIR/$PARAMCACHE ]; then
  rm -rf -f $SCRIPTDIR/$PARAMCACHE
fi
for ((i = 0; i < ${NLINES}; i++)); do
  echo ${lines[$i]} >>$SCRIPTDIR/$PARAMCACHE
done

PACKAGENAME="scunpack"

cd $SCRIPTDIR/..
BASEDIR=$PWD

# create a build area
echo "removing old build area"
rm -rf /tmp/$PACKAGENAME-build-tmp
echo "creating new build area"
mkdir /tmp/$PACKAGENAME-build-tmp
cd /tmp/$PACKAGENAME-build-tmp
TOPDIR=$PWD
echo "working in $PWD"
#ls

# we are done here, write the specs and make the rpm
cat >scunpack.spec <<EOF
Name: $PACKAGENAME$pkgsuffix
Version: $VER
Release: $REL
Summary: L1 Scouting DAQ Unpacker
License: gpl
Group: CMS/L1Scouting
Packager: scouter
Source: none
%define _tmppath $TOPDIR/scunpack-build
BuildRoot: %{_tmppath}
BuildArch: $BUILD_ARCH
AutoReqProv: no
Provides:/opt/scunpack

Requires: tbb boost-thread libcurl

%description
scouting daq unpacker

%prep

echo $RPM_SOURCE_DIR
echo $BASEDIR
cp -R $BASEDIR/src SOURCES/
cp -R $BASEDIR/include/* SOURCES/

%build
echo $RPM_SOURCE_DIR
cd SOURCES
pwd
cmake3 .
make

%install
echo $RPM_SOURCE_DIR
rm -rf \$RPM_BUILD_ROOT
mkdir -p \$RPM_BUILD_ROOT
%__install -d "%{buildroot}/var/cache/scunpack"
%__install -d "%{buildroot}/var/log/scunpack"
%__install -d "%{buildroot}/var/log/scunpack/pid"
%__install -d "%{buildroot}/opt/scunpack"
%__install -d "%{buildroot}/opt/sunpack/init.d"
%__install -d "%{buildroot}/opt/scunpack/bin"
%__install -d "%{buildroot}/opt/scunpack/lib"

cd \$RPM_BUILD_ROOT
echo "Creating directories"
mkdir -p opt/scunpack
mkdir -p etc/scunpack
mkdir -p etc/logrotate.d
mkdir -p usr/lib/systemd/system
mkdir -p etc/init.d

echo "Copying files to their destination"
cp $BASEDIR/init.d/runSCunpack.service      usr/lib/systemd/system/runSCunpack.service
cp -R $BASEDIR/*                    opt/scunpack
mkdir opt/scunpack/site-packages

pwd
cp $TOPDIR/RPMBUILD/BUILD/bin/scunpack opt/scunpack/bin/
cp $TOPDIR/RPMBUILD/BUILD/bin/scunpack opt/scunpack/python/
cp $TOPDIR/RPMBUILD/BUILD/lib/libconverter.so opt/scunpack/lib/

cp $TOPDIR/RPMBUILD/BUILD/lib/libconverter.so /usr/lib64/
cp $TOPDIR/RPMBUILD/BUILD/lib/libconverter.so /usr/lib/

echo "Deleting unnecessary files"
rm -rf opt/hltd/{bin,rpm,lib}
rm -rf opt/hltd/scripts/paramcache*
rm -rf opt/hltd/scripts/*rpm.sh

%post
mkdir -p /home/scouter/scunpack/data/unpacked
mkdir -p /home/scouter/scunpack/unpacked
useradd scouter
systemctl daemon-reload

%files
%dir %attr(777, -, -) /var/cache/scunpack
%dir %attr(777, -, -) /var/log/scunpack
%dir %attr(777, -, -) /var/log/scunpack/pid
%defattr(-, root, root, -)
/opt/scunpack/
%attr( 644 ,root, root) /usr/lib/systemd/system/runSCunpack.service

%preun
if [ \$1 == 0 ]; then
  /usr/bin/systemctl stop runSCunpack || true
  /usr/bin/systemctl disable runSCunpack || true
fi
EOF
mkdir -p RPMBUILD/{RPMS/{noarch},SPECS,BUILD,SOURCES,SRPMS}
rpmbuild --define "_topdir $(pwd)/RPMBUILD" -bb scunpack.spec

