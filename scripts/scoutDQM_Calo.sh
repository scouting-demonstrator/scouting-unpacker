#!/bin/bash
file_path_base=$1
while true
do
    file=`ls -Art $file_path_base | grep dat |  grep CALO | grep -v json | tail -n 1`
    echo $file
    FILESIZE=$(stat -c%s "$file_path_base/$file")
    echo $FILESIZE
    bzip2 -dk "$file_path_base/$file"
    unzippedFile=${file%.bz2}
    nevents=$(($FILESIZE))
    python3 skac_calo.py "$unzippedFile" $nevents "jets" "$file_path_base/"
    python3 skac_calo.py "$unzippedFile" $nevents "egammas" "$file_path_base/"
    python3 skac_calo.py "$unzippedFile" $nevents "taus" "$file_path_base/"
#   sigal build -f
    sleep 30s
done
