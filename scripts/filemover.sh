#!/usr/bin/bash
while true
do
 file=`ssh $USER@cmsusr.cern.ch ls -Art /globalscratch/l1scouting | grep dat | grep GMT | tail -n 1`
 echo $file
 scp $USER@cmsusr.cern.ch:/globalscratch/l1scouting/${file} /home/scouter/scunpack/data

 file=`ssh $USER@cmsusr.cern.ch ls -Art /globalscratch/l1scouting | grep dat | grep CALO | tail -n 1`
 echo $file
 scp $USER@cmsusr.cern.ch:/globalscratch/l1scouting/${file} /home/scouter/scunpack/data
 
 sleep 180s
done
