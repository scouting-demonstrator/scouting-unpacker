#!/bin/bash
file_path_base=$1

unzip () {
  if ! [ -z "$1" ]
  then
    file=$1
    if grep -q "bz2" <<< $file; then
      lbzip2 -dkvv "$file_path_base/$file"
    fi
  fi
}

mv_file (){
  file=$1
  cd $file_path_base
  mv "$file" "$file_path_base/unpacked"
  mv "${file%.bz2}" "$file_path_base/unpacked"
  cd -
}

while :
do
    file_calo=`ls -Art $file_path_base | grep dat | grep CALO | grep -v json | tail -n 1`
    file_calo2=`ls -Art $file_path_base | grep dat | grep CALO | grep -v json | grep -v $file_calo | tail -n 1`
    file_gmt=`ls -Art $file_path_base | grep dat | grep GMT | grep -v json | tail -n 1`
    file_gmt2=`ls -Art $file_path_base | grep dat | grep GMT | grep -v json | grep -v $file_gmt | tail -n 1`
    
    unzip $file_calo &
    unzip $file_gmt &
    unzip $file_calo2 & 
    unzip $file_gmt2 &
    wait

    if [ -z "$file_gmt" ] && [ -z "$file_calo" ]
    then 
    	echo "no files found"
	sleep 30s
	break
    fi
    
    if ! [ -z "$file_gmt" ]; then /opt/scunpack/bin/scunpack -i "$file_path_base/${file_gmt%.bz2}" -b -1 -r 50000000 ; fi &
    if ! [ -z "$file_calo" ]; then /opt/scunpack/bin/scunpack -i "$file_path_base/${file_calo%.bz2}" -b -1 -r 50000000 ; fi &
    if ! [ -z "$file_gmt2" ]; then /opt/scunpack/bin/scunpack -i "$file_path_base/${file_gmt2%.bz2}" -b -1 -r 50000000 ; fi &
    if ! [ -z "$file_calo2" ]; then /opt/scunpack/bin/scunpack -i "$file_path_base/${file_calo2%.bz2}" -b -1 -r 50000000 ; fi &
    wait

    if ! [ -z "$file_gmt" ]; then mv_file $file_gmt ; fi
    if ! [ -z "$file_calo" ]; then mv_file $file_calo ; fi
    if ! [ -z "$file_gmt2" ]; then mv_file $file_gmt2 ; fi
    if ! [ -z "$file_calo2" ]; then mv_file $file_gmt2 ; fi
    
done
