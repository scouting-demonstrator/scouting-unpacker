#!/bin/bash
file_path_base=$1
while true
do
    file=`ls -Art $file_path_base | grep dat | tail -n 1`
    echo $file
    FILESIZE=$(stat -c%s "$file_path_base/$file")
    echo $FILESIZE
    bzip2 -dk "$file_path_base/$file"
    unzippedFile=${file%.bz2}
    nevents=$(($FILESIZE/100))
    python3 skac_muon.py "$file" $nevents
    sigal build -f
    sleep 180s
done
