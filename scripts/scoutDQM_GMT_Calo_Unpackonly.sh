#!/bin/bash
file_path_base=$1
nevents=1000000000
run=360141
NEventsPerProcess=10000000

offset=0
file_calo=`ls -Art $file_path_base | grep dat |  grep CALO | grep $run | grep -v json | tail -n 1`
if [ -z "$file_calo" ]
then 
	echo "no calo file found"
    sleep 30s
    break
fi
echo $file_calo
FILESIZE=$(stat -c%s "$file_path_base/$file_calo")
echo $FILESIZE
if grep -q "bz2" <<< $file_calo; then
	bzip2 -dkvv "$file_path_base/$file_calo"
fi
unzippedFileCALO=${file_calo%.bz2}

file_gmt=`ls -Art $file_path_base | grep dat |  grep GMT | grep $run | grep -v json | tail -n 1`
if [ -z "$file_gmt" ]
then 
	echo "no gmt file found"
    sleep 30s
    break
fi
echo $file_gmt
if grep -q "bz2" <<< $file_calo; then
	bzip2 -dkvv "$file_path_base/$file_gmt"
fi
unzippedFileGMT=${file_gmt%.bz2}

let "count=0"
let "end_b=0"
let "start_b=0"

while [ $end_b -le $nevents ]
do
	let "start_b=count*NEventsPerProcess"
	let "end_b=(count+1)*NEventsPerProcess"
	echo $start_b
	echo $end_b
	python3 ../python/skac_gmt_calo_unpackonly.py "$unzippedFileGMT" "$unzippedFileCALO" "$file_path_base/" $start_b $end_b
	let "count=count+1"
done
