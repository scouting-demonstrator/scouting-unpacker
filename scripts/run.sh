#!/bin/sh
#
# This script will start SCUnpack Software
#

cd ../src

umask 000 # Files and folders we create should be world accessible

while true 
do
    echo "Starting scunpack..."
    /opt/scunpack/bin/scunpack 2>&1 | logger --tag scunpack --id -p user.debug
    ../scripts/funpack.sh /home/scouter/scunpack/data
    echo "Waiting 30 seconds..."
    for ((i=30; i>0; i=i-15)) 
    do
	echo "$i..."
	sleep 15
    done
done

