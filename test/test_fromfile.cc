#include <cstdio>
#include <cstring>
#include <fstream>
#include <iostream>
#include "include/functions.h"
#include "include/Orbit.h"

int main(int argc, char *argv[]){
  if(argc<2){
    printf("Usage test_fromfile <input-filename> \n\n\n");
    exit(2);
  }
  size_t bytes_read = 0;
  char filename[strlen(argv[1])+1];
  strcpy(filename,argv[1]);
  printf("opening file %s\n",filename);
  FILE *f=fopen(filename,"r");
  size_t size=0; /*filesize*/
  
  fseek(f, 0, SEEK_END); 
  size = ftell(f);         /*calc the size needed*/
  fseek(f, 0, SEEK_SET);
  printf("file size is %ld\n",size);
  size_t chunk = 1024*1024*4;
  unsigned char buffer [chunk]; /*buffer*/  
  unsigned char *p = buffer;
  unsigned int offset=0;
  size_t status = 0;
  while(true){
    status = fread(buffer+offset, sizeof(unsigned char), chunk-offset, f);
    bytes_read+= status;
    printf("fread returned with status %ld\n",status);
    printf("bytes left on file %ld\n",size-bytes_read);
    if(status==0) break;
    if(status != chunk-offset)
      printf("ERROR, bytes read %ld, bytes asked %ld\n",status, chunk-offset);
    p = buffer;
    while(p-buffer<(chunk-76)){
      // printf("p-buffer %ld  chunk %ld \n",p-buffer,chunk); 
      ugmt::Orbit b = ugmt::convert(p,chunk+buffer-p);
      // printf("read orbit %d with %lu bxs\n",b.getOrbit(),b.bunchCrossings().size());
    }
    offset = chunk+buffer-p;
    // printf("copying back leftover %d bytes\n",offset);
    memcpy(buffer,p,offset);
  }
  fclose(f);
  return 0;
}
