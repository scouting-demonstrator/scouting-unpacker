#include <iostream>
#include <iomanip>
#include <fstream>
#include <cstdio>
#include <cstdint>
#include <string>
#include <cmath>
#include <vector>
#include <cmath>

#include <arrow/api.h>
#include <arrow/io/api.h>
#include "arrow/io/file.h"
#include <parquet/arrow/writer.h>
#include <parquet/stream_writer.h>
#include <arrow/ipc/api.h>
#include <parquet/arrow/reader.h>
#include <parquet/arrow/writer.h>

#include "tools.h"
#include "converters.h"
#include "scouting-headers/masks.h"
#include "scouting-headers/scales.h"
#include "scouting-headers/blocks.h"
#include "scouting-headers/arrow_blocks.h"



//------------------------------------------------------------------------------
//
// uGMT RAW data converter to Parquet
//
//------------------------------------------------------------------------------
uint64_t ugmt::skimAndConvert(ugmt::arrow_data_block &adblock,
                              const char *filename,
                              const char *ofilename,
                              int64_t nblocks,
                              uint32_t npqtrows,
                              int ptcut,
                              int qualcut,
                              bool excludeIntermediate,
                              bool excludeFinal)
{

    uint32_t orbit       = 0;
    uint32_t bx          = 0;
    uint32_t block_count = 0;
    uint64_t discarded   = 0;
    uint64_t full        = 0;
    uint64_t eventcount  = 0;
    uint64_t dimuoncount = 0;
    uint64_t countA      = 0;
    int64_t  ibytes      = 0;
    uint32_t header;

    // open file
    FILE *f = fopen(filename, "r");
    // if NULL, throw error
    if (f == NULL) {
        perror("ERROR opening input file");
        return countA;
    }

    // declare block to read
    ugmt::block bl;

    // file size
    fseek(f, 0, SEEK_END);
    int64_t lSize = ftell(f);
    rewind(f);
    std::cout << "File size: "
              << std::setw(12) << lSize
              << std::endl;


    // read until header is not empty (end of file)
    while (ibytes = fread(&header, 1, sizeof(uint32_t), f)) {
        // sum header size
        countA += ibytes;

        // detect end of file and exit while
        // if (countA > (lSize - 2048)) {
        if (countA >= lSize) {
            std::cout << "Close file (EOF 1)" << std::endl;
            fclose(f);
            break;
        }
        if (ferror(f)) {
            fclose(f);
            std::cout << "Close file (ferror)" << std::endl;
            break;
        }
        if (feof(f)) {
            fclose(f);
            std::cout << "Close file (EOF 2)" << std::endl;
            break;
        }

        // count mA and mB
        uint32_t mAcount = (header & header_masks::mAcount) >> header_shifts::mAcount;
        uint32_t mBcount = (header & header_masks::mBcount) >> header_shifts::mBcount;

        // read block
        ibytes = fread(&bl, 1, 8+(mAcount+mBcount)*12, f);
        countA += ibytes;

        // get orbit and bx number of block
        orbit = bl.orbit & 0x7FFFFFFF;
        bx    = bl.bx;

        // check if block has the maximum possible amount of muons
        if (mAcount+mBcount==16) full+=1;

        // get muons in block (final and intermediate)
        for (unsigned int i=0; i<mAcount+mBcount; i++) {
            uint32_t interm = (bl.mu[i].extra >> ugmt::shiftsMuon::interm) & ugmt::masksMuon::interm;

            // exclude intermediate and/or final muons, if requested
            if (excludeIntermediate && (interm == 1)) continue;
            if (excludeFinal        && (interm == 0)) continue;

            // remove intermediate if required
            // index==0 and ietaext==0 are a necessary and sufficient condition
            uint32_t index    = (bl.mu[i].s >> ugmt::shiftsMuon::index)  & ugmt::masksMuon::index;
            uint32_t ietaextu = (bl.mu[i].f >> ugmt::shiftsMuon::etaext) & ugmt::masksMuon::etaextv;
            int32_t ietaext;
            if (((bl.mu[i].f >> ugmt::shiftsMuon::etaext) & ugmt::masksMuon::etaexts)!=0) {
                ietaext = ietaextu -= 256;
            } else {
                ietaext = ietaextu;
            }

            // extract pt and quality and apply cut if required
            int32_t iptuncon = (bl.mu[i].s >> ugmt::shiftsMuon::ptuncon) & ugmt::masksMuon::ptuncon;
            int32_t ipt      = (bl.mu[i].f >> ugmt::shiftsMuon::pt)      & ugmt::masksMuon::pt;
            if ((ipt-1) < ptcut) {
                discarded++;
                continue;
            }
            uint32_t qual = (bl.mu[i].f >> ugmt::shiftsMuon::qual) & ugmt::masksMuon::qual;
            if (qual < qualcut) {
                discarded++;
                continue;
            }

            // extract integer value for extrapolated phi
            int32_t iphiext = ((bl.mu[i].f >> ugmt::shiftsMuon::phiext) & ugmt::masksMuon::phiext);

            // extract integer value for extrapolated phi
            int32_t idxy = ((bl.mu[i].s >> ugmt::shiftsMuon::dxy) & ugmt::masksMuon::dxy);

            // extract iso bits and charge
            uint32_t iso = (bl.mu[i].s >> ugmt::shiftsMuon::iso) & ugmt::masksMuon::iso;
            int32_t chrg = 0;
            if ((bl.mu[i].s >> ugmt::shiftsMuon::chrgv) & ugmt::masksMuon::chrgv==1)
                chrg=(bl.mu[i].s >> ugmt::shiftsMuon::chrg) & ugmt::masksMuon::chrg==1 ? -1 : 1 ;

            // extract eta and phi at muon station
            int32_t iphi   = (bl.mu[i].s >> ugmt::shiftsMuon::phi)      & ugmt::masksMuon::phi;
            uint32_t ieta1 = (bl.mu[i].extra >> ugmt::shiftsMuon::eta1) & ugmt::masksMuon::eta;
            uint32_t ieta2 = (bl.mu[i].extra >> ugmt::shiftsMuon::eta2) & ugmt::masksMuon::eta;


            uint32_t ieta_u;
            int32_t ieta;
            // checking if raw eta should be taken from muon 1 or muon 2
            if ( (bl.mu[i].extra & 0x1) == 0 ) {
                ieta_u = ieta1;
            } else {
                ieta_u = ieta2;
            }

            // two's complement
            if ( ieta_u > 256 ) {
                ieta = ieta_u - 512;
            } else {
                ieta = ieta_u;
            }

            float fpt      = (ipt     -1) * ugmt::scales::pt_scale;                 // -1 since bin 0 is for invalid muons
            float fptuncon = (iptuncon-1) * ugmt::scales::ptunconstrained_scale;    // -1 since bin 0 is for invalid muons
            float fphi     = iphi         * ugmt::scales::phi_scale;
            float fphiext  = iphiext      * ugmt::scales::phi_scale;
            float feta     = ieta         * ugmt::scales::eta_scale;
            float fetaext  = ietaext      * ugmt::scales::eta_scale;

            if (fphiext>M_PI) fphiext = fphiext - 2.*M_PI;
            if (fphi   >M_PI) fphi    = fphi    - 2.*M_PI;

            // append to arrow arrays
            adblock.Append(orbit, bx, interm, fpt, fptuncon, chrg, iso, index, qual, fphi, fphiext, feta, fetaext, idxy);

            eventcount++;
        }
        block_count++;

        // if max number of packets is given in cmd line, detect when to stop
        if (nblocks>0) {
            // if max number of blocks requested is reached...
            if (block_count >= nblocks) {
                // ..., signal it, close the input file, write parquet table on output file and return
                std::cerr << "Max number of blocks reached. Closing input file..." << std::endl;
                fclose(f);
                break;
            }
        }

        if (block_count%100000==0) {
            std::cout << "Block:"
                      << std::setw(10) << block_count << "    "
                      << "BXs discarded (0 muons):"
                      << std::setw(10) << discarded   << "    "
                      << "BXs w/ full record (16 muons):"
                      << std::setw(10) << full        << "    "
                      << "Muon entries written:"
                      << std::setw(10) << eventcount  << std::endl;
        }
    }

    // fill array data for Parquet table and return
    adblock.Finish();

    return countA;
}
//------------------------------------------------------------------------------





//------------------------------------------------------------------------------
//
// Demux RAW data converter to Parquet
//
//------------------------------------------------------------------------------
uint64_t demux::skimAndConvert(demux::arrow_data_block &adblock,
                               const char *filename,
                               const char *ofilename,
                               int64_t nblocks,
                               uint32_t npqtrows)
{

    int32_t  ibytes      = 0;
    uint32_t orbit       = 0;
    uint32_t bx          = 0;
    uint64_t count       = 0;
    uint64_t countA      = 0;
    uint64_t jetcount    = 0;
    uint64_t egammacount = 0;
    uint64_t taucount    = 0;
    uint64_t sumcount    = 0;
    std::vector<uint64_t> calo_cuts_vect_bx(4,0);

    // open file
    FILE *f = fopen(filename, "r");
    // if NULL, throw error
    if (f == NULL) {
        perror("ERROR opening input file");
        return countA;
    }

    // declare block to read
    demux::block bl;

    // file size
    fseek(f, 0, SEEK_END);
    int64_t lSize = ftell(f);
    rewind(f);
    std::cout << "File size: "
              << std::setw(12) << lSize
              << std::endl;


    // read until block is not empty (end of file)
    while (ibytes = fread(&bl, 1, sizeof(demux::block), f)) {

        if (countA > ((lSize-2) / sizeof(demux::block))) {
            std::cout << "Close file (EOF 1)" << std::endl;
            fclose(f);
            break;
        }
        if (ferror(f)) {
            fclose(f);
            std::cout << "Close file (ferror)" << std::endl;
            break;
        }
        if (feof(f)) {
            fclose(f);
            std::cout << "Close file (EOF 2)" << std::endl;
            break;
        }

        orbit = bl.orbit;
        bx    = bl.bx;
        std::vector<int32_t> vET, vEta, vPhi, vIso, vType;
        std::vector<int32_t> vETEt, vHTEt, vETmissEt, vHTmissEt, vETmissPhi, vHTmissPhi;
        std::vector<bool> calo_cuts_vect(4, 0);
        uint32_t itr_cntr = 0;
        uint32_t link     = 0;


        for (uint32_t i=0; i<56; i++) {
            if (i%7==0) {
                link = bl.frame[i];
                continue;
            }


            int32_t ET(0), Eta(0), Phi(0), Iso(0);
            int32_t ETEt(0), ETEttem(0), ETMinBiasHF(0),
                     HTEt(0), HTtowerCount(0), HTMinBiasHF(0),
                     ETmissEt(0), ETmissPhi(0), ETmissASYMET(0), ETmissMinBiasHF(0),
                     HTmissEt(0), HTmissPhi(0), HTmissASYMHT(0), HTmissMinBiasHF(0),
                     ETHFmissEt(0), ETHFmissPhi(0), ETHFmissASYMETHF(0), ETHFmissCENT(0),
                     HTHFmissEt(0), HTHFmissPhi(0), HTHFmissASYMHTHF(0), HTHFmissCENT(0);

            switch ((link)/2) {
                // egamma
                case 1: {
                    ET  = ((bl.frame[i] >> demux::shiftsEGamma::ET)  & demux::masksEGamma::ET);
                    if (ET==0) break;
                    Eta = ((bl.frame[i] >> demux::shiftsEGamma::eta) & demux::masksEGamma::eta);
                    Phi = ((bl.frame[i] >> demux::shiftsEGamma::phi) & demux::masksEGamma::phi);
                    Iso = ((bl.frame[i] >> demux::shiftsEGamma::iso) & demux::masksEGamma::iso);
                    egammacount++;
                    break;
                }

                // jets
                case 0: {
                    ET  = ((bl.frame[i] >> demux::shiftsJet::ET)  & demux::masksJet::ET);
                    if (ET==0) break;
                    Eta = ((bl.frame[i] >> demux::shiftsJet::eta) & demux::masksJet::eta);
                    Phi = ((bl.frame[i] >> demux::shiftsJet::phi) & demux::masksJet::phi);
                    Iso = 0; // not defined, but want to use same structure for all three objects
                    jetcount++;
                    break;
                }

                // taus
                case 3: {
                    ET  = ((bl.frame[i] >> demux::shiftsTau::ET)  & demux::masksTau::ET);
                    if (ET==0) break;
                    Eta = ((bl.frame[i] >> demux::shiftsTau::eta) & demux::masksTau::eta);
                    Phi = ((bl.frame[i] >> demux::shiftsTau::phi) & demux::masksTau::phi);
                    Iso = ((bl.frame[i] >> demux::shiftsTau::iso) & demux::masksTau::iso);
                    taucount++;
                    break;
                }

                // energy sums
                case 2: {
                    if (link==4) break;
                    ET = 0; Eta = 0; Phi = 0; Iso = 0;
                    itr_cntr++;

                    switch(itr_cntr-1) {

                        // ET
                        case 0: {
                            ETEt        = ((bl.frame[i] >> demux::shiftsESums::ETEt)        & demux::masksESums::ETEt);
                            if (ETEt==0) break;
                            ETEttem     = ((bl.frame[i] >> demux::shiftsESums::ETEttem)     & demux::masksESums::ETEttem);
                            ETMinBiasHF = ((bl.frame[i] >> demux::shiftsESums::ETMinBiasHF) & demux::masksESums::ETMinBiasHF);
                            sumcount++;
                            break;
                        }

                        // HT
                        case 1: {
                            HTEt         = ((bl.frame[i] >> demux::shiftsESums::HTEt)         & demux::masksESums::HTEt);
                            if (HTEt==0) break;
                            HTtowerCount = ((bl.frame[i] >> demux::shiftsESums::HTtowerCount) & demux::masksESums::HTtowerCount);
                            HTMinBiasHF  = ((bl.frame[i] >> demux::shiftsESums::HTMinBiasHF)  & demux::masksESums::HTMinBiasHF);
                            sumcount++;
                            break;
                        }

                        // ETMiss
                        case 2: {
                            ETmissEt        = ((bl.frame[i] >> demux::shiftsESums::ETmissEt)        & demux::masksESums::ETmissEt);
                            if (ETmissEt==0) break;
                            ETmissPhi       = ((bl.frame[i] >> demux::shiftsESums::ETmissPhi)       & demux::masksESums::ETmissPhi);
                            ETmissASYMET    = ((bl.frame[i] >> demux::shiftsESums::ETmissASYMET)    & demux::masksESums::ETmissASYMET);
                            ETmissMinBiasHF = ((bl.frame[i] >> demux::shiftsESums::ETmissMinBiasHF) & demux::masksESums::ETmissMinBiasHF);
                            sumcount++;
                            break;
                        }

                        // HTMiss
                        case 3: {
                            HTmissEt        = ((bl.frame[i] >> demux::shiftsESums::HTmissEt)        & demux::masksESums::HTmissEt);
                            if (HTmissEt==0) break;
                            HTmissPhi       = ((bl.frame[i] >> demux::shiftsESums::HTmissPhi)       & demux::masksESums::HTmissPhi);
                            HTmissASYMHT    = ((bl.frame[i] >> demux::shiftsESums::HTmissASYMHT)    & demux::masksESums::HTmissASYMHT);
                            HTmissMinBiasHF = ((bl.frame[i] >> demux::shiftsESums::HTmissMinBiasHF) & demux::masksESums::HTmissMinBiasHF);
                            sumcount++;
                            break;
                        }

                        // ETHFMiss
                        case 4: {
                            ETHFmissEt       = ((bl.frame[i] >> demux::shiftsESums::ETHFmissEt)       & demux::masksESums::ETHFmissEt);
                            if (ETHFmissEt==0) break;
                            ETHFmissPhi      = ((bl.frame[i] >> demux::shiftsESums::ETHFmissPhi)      & demux::masksESums::ETHFmissPhi);
                            ETHFmissASYMETHF = ((bl.frame[i] >> demux::shiftsESums::ETHFmissASYMETHF) & demux::masksESums::ETHFmissASYMETHF);
                            ETHFmissCENT     = ((bl.frame[i] >> demux::shiftsESums::ETHFmissCENT)     & demux::masksESums::ETHFmissCENT);
                            sumcount++;
                            break;
                        }

                        // HTHFMiss
                        case 5: {
                            HTHFmissEt       = ((bl.frame[i] >> demux::shiftsESums::HTHFmissEt)       & demux::masksESums::HTHFmissEt);
                            if (HTHFmissEt==0) break;
                            HTHFmissPhi      = ((bl.frame[i] >> demux::shiftsESums::HTHFmissPhi)      & demux::masksESums::HTHFmissPhi);
                            HTHFmissASYMHTHF = ((bl.frame[i] >> demux::shiftsESums::HTHFmissASYMHTHF) & demux::masksESums::HTHFmissASYMHTHF);
                            HTHFmissCENT     = ((bl.frame[i] >> demux::shiftsESums::HTHFmissCENT)     & demux::masksESums::HTHFmissCENT);
                            sumcount++;
                            break;
                        }

                        default:
                            std::cout << "WARNING:: invalid counter value for ESUMs words, should be (0,5)" << std::endl;
                            break;
                    }

                    break;
                }

                default:
                    std::cout << "WARNING:: out of range link number" << std::endl;
            }

            if ((ET != 0) || ((link/2) == 2)) {
                vET.push_back(ET);
                vEta.push_back(Eta);
                vPhi.push_back(Phi);
                vIso.push_back(Iso);
                vType.push_back(link/2);
                vETEt.push_back(ETEt);
                vHTEt.push_back(HTEt);
                vETmissEt.push_back(ETmissEt);
                vHTmissEt.push_back(HTmissEt);
                vETmissPhi.push_back(ETmissPhi);
                vHTmissPhi.push_back(HTmissPhi);
            }

            float jetCut    = 80;
            float egammaCut = 20;
            float tauCut    = 80;
            float esumsCut  = 511;

            // cut jets
            if (((link/2) == 0) && (ET > jetCut)) {
                calo_cuts_vect.at(0) = true;
            }

            // cut egammas
            if (((link/2) == 1) && (ET > egammaCut)) {
                calo_cuts_vect.at(1) = true;
            }

            // cut sums
            if (((link/2) == 2) && (ETEt > esumsCut)) {
                calo_cuts_vect.at(2) = true;
            }

            // cut taus
            if (((link/2) == 3) && (ET > tauCut)) {
                calo_cuts_vect.at(3) = true;
            }

            if ((i==55) && (vET.size() > 0)) {
                for(uint32_t k = 0; k < vET.size(); k++) {

                    // two's complement
                    if (vEta[k] > 127) {
                        vEta[k] = vEta[k] - 256;
                    }


                    float fphi = vPhi[k] * demux::scales::phi_scale;
                    float feta = vEta[k] * demux::scales::eta_scale;
                    float fET  = vET[k]  * demux::scales::et_scale;

                    float fETEt      = vETEt[k]      * demux::scales::et_scale;
                    float fHTEt      = vHTEt[k]      * demux::scales::et_scale;
                    float fETmissEt  = vETmissEt[k]  * demux::scales::et_scale;
                    float fHTmissEt  = vHTmissEt[k]  * demux::scales::et_scale;
                    float fETmissPhi = vETmissPhi[k] * demux::scales::phi_scale;
                    float fHTmissPhi = vHTmissPhi[k] * demux::scales::phi_scale;

                    if (fphi>M_PI)       fphi       = fphi       - 2.*M_PI;
                    if (fETmissPhi>M_PI) fETmissPhi = fETmissPhi - 2.*M_PI;
                    if (fHTmissPhi>M_PI) fHTmissPhi = fHTmissPhi - 2.*M_PI;

                    adblock.Append(orbit, bx, fET, vType.at(k), feta, fphi, vIso.at(k), fETEt, fHTEt, fETmissEt, fHTmissEt, fETmissPhi, fHTmissPhi);

                    count++;
                }
            }

            if (i == 55) {
                vET.clear();
                vEta.clear();
                vPhi.clear();
                vIso.clear();
                vType.clear();
                vETEt.clear();
                vHTEt.clear();
                vETmissEt.clear();
                vHTmissEt.clear();
                vETmissPhi.clear();
                vHTmissPhi.clear();

                for (uint32_t l = 0; l < 4; l++) {
                    if (calo_cuts_vect.at(l)) calo_cuts_vect_bx.at(l)++;
                }
            }
        }
        countA++;


        // log some interesting counts
        if (countA%100000==0) {
            std::cout << "Block:"
                    << std::setw(12) << countA      << "    "
                    << "Objects count:"
                    << std::setw(12) << count       << "    "
                    << "Jets:"
                    << std::setw(12) << jetcount    << "    "
                    << "EGammas:"
                    << std::setw(12) << egammacount << "    "
                    << "Tau:"
                    << std::setw(12) << taucount    << "    "
                    << "Sums:"
                    << std::setw(12) << sumcount    << std::endl;
        }

        // if max number of packets is given in cmd line, detect when to stop
        if (nblocks>0) {
            // if max number of blocks requested is reached...
            if (countA >= nblocks) {
                // ..., signal it, close the input file, write parquet table on output file and return
                std::cerr << "Max number of blocks reached. Closing input file..." << std::endl;
                fclose(f);
                break;
            }
        }
    }

    // print some useful information
    for (uint32_t l = 0; l < 4; l++){
        std::cout << "N passing cut at " << l << " = " << std::setw(12) << calo_cuts_vect_bx.at(l) << std::endl;
    }

    // fill array data for Parquet table and return
    adblock.Finish();

    return countA;
}
//------------------------------------------------------------------------------





//------------------------------------------------------------------------------
//
// uGT RAW data converter to Parquet
//
//------------------------------------------------------------------------------
uint64_t ugt::skimAndConvert(ugt::arrow_data_block &adblock,
                             const char *filename,
                             const char *ofilename,
                             int64_t nblocks,
                             uint32_t npqtrows)
{

    int32_t  ibytes        = 0;
    uint32_t orbit         = 0;
    uint32_t bx            = 0;
    uint64_t packet_count  = 0;
    uint64_t bx_count      = 0;
    uint64_t countA        = 0;
    uint64_t subfile_index = 0;         // needed at the moment due to big RAW data files
    bool     bx_empty      = false;
    uint32_t orbits_seen;
    uint32_t orbits_dropped;

    // open file
    FILE *f = fopen(filename, "r");
    if (f == NULL) {
        perror("ERROR opening input file");
        return countA;
    }

    // declare block to read
    ugt::block bl;
    ugt::header hd;
    ugt::trailer tl;

    // file size
    fseek(f, 0, SEEK_END);
    int64_t lSize = ftell(f);
    rewind(f);
    std::cout << "File size: "
              << std::setw(12) << lSize
              << std::endl;


    // read until block is not empty (end of file)
    while (ibytes = fread(&hd, 1, sizeof(ugt::header), f)) {
        // sum header size
        countA += ibytes;

        // check if header is missing
        if (hd.frame[0]!=0xfeedbeef) {
            while (hd.frame[0]!=0xfeedbeef) {
                printf("Missing header    0x%08x_%08x_%08x_%08x_%08x_%08x_%08x_%08x\n", hd.frame[0], hd.frame[1], hd.frame[2], hd.frame[3], hd.frame[4], hd.frame[5], hd.frame[6], hd.frame[7]);
                ibytes = fread(&hd, 1, sizeof(ugt::header), f);
                countA += ibytes;
            }
        }

        // read BX blocks...
        while (ibytes = fread(&bl, 1, sizeof(ugt::block), f)) {

            // ...until the trailer is found
            if (bl.frame[0]==0xbeefdead) {
                fseek(f, countA, SEEK_SET);
                ibytes = fread(&tl, 1, sizeof(ugt::trailer), f);
                countA += ibytes;

                // get "orbits seen by firmware" counter (no absolute orbit number at the moment)
                orbits_seen    = tl.frame[142];
                orbits_dropped = tl.frame[140];

                // get bx bit map and orbit seen number
                std::vector<uint16_t> bx_vect;
                for (uint32_t i = 0; i < (14*8); i++) {                 // 14*8 = 14 frames, 8 links of orbit trailer containing BX hitmap
                    bit_check(&bx_vect, tl.frame[8 + i], (i*32 + 1));   // +1 added to account for BX counting starting at 1
                }

                // if no strobed bx's in orbit, add dummy row in table (with bx=0)
                if (bx_vect.size()==0) {
                    bx_empty = true;
                    std::vector<bool> algobits(512, false);
                    adblock.Append(orbits_seen, orbits_dropped, 0, algobits);
                } else {
                    bx_empty = false;
                }

                // go back to payload of orbit
                fseek(f, countA-sizeof(ugt::trailer)-bx_vect.size()*sizeof(ugt::block), SEEK_SET);

                // get blocks and fill columns to write in Parquet file
                for (uint32_t i = 0; i < bx_vect.size(); i++) {
                    fread(&bl, 1, sizeof(ugt::block), f);

                    // loop over first 512 bits in bx block
                    std::vector<bool> algobits(512, false);
                    for (uint32_t j = 0; j < 16; j++) {
                        uint32_t word = bl.frame[j];
                        uint32_t ll = 0;

                        // to partially fix the bugs in the zs_ugt.vhd
                        switch (j) {
                            case 0 : ll = 12; break;
                            case 1 : ll = 13; break;
                            case 2 : ll = 14; break;
                            case 3 : ll = 15; break;
                            case 4 : ll =  4; break;
                            case 5 : ll =  5; break;
                            case 6 : ll =  6; break;
                            case 7 : ll =  7; break;
                            case 8 : ll =  8; break;
                            case 9 : ll =  9; break;
                            case 10: ll = 10; break;
                            case 11: ll = 11; break;
                            case 12: ll =  0; break;
                            case 13: ll =  1; break;
                            case 14: ll =  2; break;
                            case 15: ll =  3; break;
                            default: ll =  j; break;
                        }

                        // set to 0 words affected by issue #82
                        // https://gitlab.cern.ch/scouting-demonstrator/scouting-preprocessor/-/issues/82
                        if (ll!=12 && ll!=13) word = 0;

                        // fill algo bits builders
                        for (uint32_t k = 0; k < 32; k++) {
                            bool algo = word & 1;
                            algobits[ll*32+k] = algo;
                            word >>= 1;
                        }
                    }

                    adblock.Append(orbits_seen, orbits_dropped, bx_vect[i], algobits);
                }

                bx_count += bx_vect.size();

                fseek(f, countA, SEEK_SET);

                break;
            }

            countA += ibytes;
        }


        // log status of unpacker
        if (packet_count%100000==0) {
            std::cout << "Packet:"
                      << std::setw(10) << packet_count << "    "
                      << "BXs:"
                      << std::setw(10) << bx_count     << std::endl;
        }

        packet_count++;

        // if max number of packets is given in cmd line, detect when to stop
        if (nblocks > 0) {
            // if max number of blocks requested is reached...
            if (packet_count >= nblocks) {
                // ..., signal it, close the input file, write parquet table on output file and return
                std::cerr << "Max number of blocks reached. Closing input file..." << std::endl;
                fclose(f);
                break;
            }
        }
    }

    // fill array data for Parquet table and return
    adblock.Finish();

    return countA;
}
//------------------------------------------------------------------------------
