#include <include/Orbit.h>
#include <include/builder.h>
#include <arrow/util/logging.h>

#include <iostream>

ugmt::builder::builder(std::shared_ptr<arrow::DataType> muon, std::shared_ptr<arrow::DataType> bx) :
  pool(arrow::default_memory_pool()), f_builder(std::make_shared<arrow::UInt32Builder>(pool)),
  s_builder(std::make_shared<arrow::UInt32Builder>(pool)), muon_field_builders({f_builder,s_builder}),
  muon_builder(std::make_shared<arrow::StructBuilder>(muon,pool,muon_field_builders)),
  muons_builder(std::make_shared<arrow::ListBuilder>(pool,muon_builder)),
  bxn_builder(std::make_shared<arrow::UInt32Builder>(pool)),
  bunch_crossing_component_builders({bxn_builder,muons_builder}),
  bunch_crossing_builder(std::make_shared<arrow::StructBuilder>(bx,pool,bunch_crossing_component_builders)),
  bx_list_builder(std::make_shared<arrow::ListBuilder>(pool,bunch_crossing_builder)),
  orbitn_builder(std::make_shared<arrow::UInt32Builder>(pool)){
}
arrow::Status ugmt::builder::append(ugmt::Orbit &o){

  ARROW_CHECK_OK(orbitn_builder->Append(o.getOrbit()));
  ARROW_CHECK_OK(bx_list_builder->Append());
  for(auto b : o.bunchCrossings()){
    ARROW_CHECK_OK(bunch_crossing_builder->Append());
    ARROW_CHECK_OK(bxn_builder->Append(b.getBx()));
    ARROW_CHECK_OK(muons_builder->Append());
    for (auto m : b.getMuons()){
      ARROW_CHECK_OK(muon_builder->Append());
      ARROW_CHECK_OK(f_builder->Append(m.f));
      ARROW_CHECK_OK(s_builder->Append(m.s));
    }
  }
  return arrow::Status::OK();

}

std::shared_ptr<arrow::Table> ugmt::builder::finish(std::shared_ptr<arrow::Schema> schema){
  std::shared_ptr<arrow::Array> orbitn_array;
  orbitn_builder->Finish(&orbitn_array);
  std::shared_ptr<arrow::Array> bx_list_array;
  bx_list_builder->Finish(&bx_list_array);
  
  
  std::shared_ptr<arrow::Table> table = arrow::Table::Make(schema, {orbitn_array, bx_list_array});
  std::cout << table->ToString() << std::endl;    
  return table;
}
