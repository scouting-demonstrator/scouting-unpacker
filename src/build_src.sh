#!/bin/bash
PYTHONPATH=$1
wget https://gitlab.cern.ch/scouting-demonstrator/scdaq/-/raw/master/src/format.h?inline=false
mv 'format.h?inline=false' format.h
c++ -fPIC -std=c++11 -I $PYTHONPATH -o scunpack.so -shared skimAndConvert.cc -g
#cp scunpack.so ../python/
