#include "tools.h"

#include <iostream>
#include <getopt.h>

#include <arrow/api.h>
#include <arrow/io/api.h>
#include <parquet/arrow/writer.h>
#include <parquet/stream_writer.h>
#include <parquet/arrow/writer.h>



// Write out an arrow Table as a Parquet file
void write_parquet_file(const arrow::Table& table, const char *ofilename, uint32_t npqtrows) {
    std::shared_ptr<arrow::io::FileOutputStream> outfile;
    ARROW_ASSIGN_OR_RAISE(outfile, arrow::io::FileOutputStream::Open(ofilename));
    // The last argument to the function call is the size of the RowGroup in
    // the parquet file. Normally you would choose this to be rather large but
    // for the example, we use a small value to have multiple RowGroups.

    std::cout << "Writing parquet output file..." << std::endl;
    PARQUET_THROW_NOT_OK(parquet::arrow::WriteTable(table, arrow::default_memory_pool(), outfile, npqtrows));
    std::cout << "+ Done" << std::endl;
}



// get cmd line int argument
uint64_t getopt_integer(char *optarg)
{
    int rc;
    uint64_t value;

    rc = sscanf(optarg, "0x%lx", &value);
    if (rc <= 0)
        rc = sscanf(optarg, "%lu", &value);

    return value;
}



// Loops over each word in the orbit trailer BX map and fills a vector with the non-empty BX values
void bit_check(std::vector<uint16_t> *bx_vect, uint32_t word, uint32_t offset) {
    for (uint32_t i = 0; i<32; i++) {
        if (word & 1) {
            bx_vect->push_back(i + offset);
        }
        word >>= 1;
    }
    return;
}