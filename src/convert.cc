#include "include/low-level.h"
#include "include/format.h"
#include "include/BunchCrossing.h"
#include "include/Orbit.h"
#include "include/functions.h"
#include <vector>
#include <cstdio>

using namespace ugmt;

Orbit ugmt::convert(unsigned char *&buf, size_t size){
  std::vector<BunchCrossing> bxv;
  uint32_t orbit = 0;
  while(size>76){
    uint32_t header = *(uint32_t*)buf;
    // printf("header 0x%X\n",header);
    uint32_t mAcount = (header & format::header_masks::mAcount)>>format::header_shifts::mAcount;
    uint32_t mBcount = (header & format::header_masks::mBcount)>>format::header_shifts::mBcount;
    // printf("A %d, B %d\n",mAcount,mBcount);
    buf += sizeof(uint32_t); //advance pointer to start of block;
    size -= sizeof(uint32_t);
    // printf("bunch crossing 0x%X\n",*(uint32_t*)buf);
    // printf("orbit 0x%X\n",*(uint32_t*)(buf+4));
    const format::bunch_crossing *mybx = (format::bunch_crossing*)buf;
    if(orbit==0){
      orbit = mybx->orbit;
    }
    else{
      if(mybx->orbit!=orbit){
	buf -= sizeof(uint32_t); //rewins pointer to header;
	break;
      }
    }
    bxv.push_back(BunchCrossing(mybx->bx,mAcount+mBcount,mybx->mu));
    buf += 8+(mAcount+mBcount)*8; //advance pointer to next block;
    size -= 8+(mAcount+mBcount)*8;
    // printf("size left %ld\n\n",size);
  }
  return Orbit(orbit,bxv);
}
