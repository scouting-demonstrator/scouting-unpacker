#include <iostream>
#include <arrow/api.h>
#include <include/functions.h>

using namespace arrow;
std::shared_ptr<arrow::Schema> ugmt::makeschema(){

  std::shared_ptr<Field> f = field("f", uint32());
  std::shared_ptr<Field> s = field("s", uint32());
  const std::vector<std::shared_ptr<Field> > muon_fields = {f,s};
  
  std::shared_ptr<DataType> muon = struct_(muon_fields);
  std::shared_ptr<DataType> muon_list = list(muon);
  std::shared_ptr<Field> muons = field("muons", muon_list);

  std::shared_ptr<Field> bxn = field("bx", uint32());
  
  const std::vector<std::shared_ptr<Field> > bx_fields = {bxn,muons};
  //  std::shared_ptr<Field> bx = field("bunch_crossing", struct_(bx_fields));
  std::shared_ptr<DataType> bx = struct_(bx_fields);
  std::shared_ptr<Field> orbitn = field("orbit", uint32());
  std::shared_ptr<DataType> bx_list_type = list(bx);
  std::shared_ptr<Field> bx_list = field("bunch_crossings", bx_list_type);
  const std::vector<std::shared_ptr<Field> > orbit_fields = {orbitn,bx_list};
  std::shared_ptr<arrow::Schema> schema = std::make_shared<arrow::Schema>(orbit_fields);
  std::cout << schema->ToString() << std::endl;
  return schema;

}
