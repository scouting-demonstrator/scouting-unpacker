#include <vector>
#include <string>
#include <Python.h> //should point to your own copy of this file, or use include at compilation
#include <cstdio>
#include <cstdint>
#include <cmath>
#include <string.h>
#include <vector>
#include <fstream>
#include <iostream>
#include <cmath>
#include "format.h"


struct calo_objects_hw_data_block{
    std::vector<int> vorbit;
    std::vector<int> vbx;
    std::vector<int> vET;
    std::vector<int> vType;
    std::vector<int> vEta;
    std::vector<int> vPhi;
    std::vector<int> vIso;
    std::vector<int> vETEt;
    std::vector<int> vHTEt;
    std::vector<int> vETmissEt;
    std::vector<int> vHTmissEt;
    std::vector<int> vETmissPhi;
    std::vector<int> vHTmissPhi;
    unsigned int size(){return vET.size();}
    bool empty(){return vET.empty();}
    static const unsigned int ncols =  13;
    static std::vector<std::string> columns;
    std::vector<int> iAt(int i){
	int eta_s = vEta[i];

	if ( eta_s > 127 ){
	    eta_s = eta_s - 256;
	}

	return {vorbit[i],  
	    vbx[i],
	    vET[i],
	    vType[i],
	    eta_s,
	    vPhi[i],
	    vIso[i],
	    vETEt[i],
	    vHTEt[i],
	    vETmissEt[i],
	    vHTmissEt[i],
	    vETmissPhi[i],
	    vHTmissPhi[i]
	};
    }
    std::vector<float> fAt(int i){
	int eta_s = vEta[i];
	if ( eta_s > 127 ){ 
	    eta_s = eta_s - 256;
	}

	float phi = vPhi[i]*calo_scales::phi_scale;
	float eta = eta_s*calo_scales::eta_scale;
	float et = vET[i]*calo_scales::et_scale;

	float ETEt = vETEt[i]*calo_scales::et_scale;
	float HTEt = vHTEt[i]*calo_scales::et_scale;
	float ETmissEt = vETmissEt[i]*calo_scales::et_scale;
	float HTmissEt = vHTmissEt[i]*calo_scales::et_scale;
	float ETmissPhi = vETmissPhi[i]*calo_scales::phi_scale;
	float HTmissPhi = vHTmissPhi[i]*calo_scales::phi_scale;

	if(ETmissPhi>2*M_PI){ ETmissPhi=ETmissPhi-2.*M_PI; }
	if(HTmissPhi>2*M_PI){ HTmissPhi=HTmissPhi-2.*M_PI; }
	if(phi>2*M_PI){ phi=phi-2.*M_PI; }

	return {float(vorbit[i]),  
	    float(vbx[i]),
	    et,
	    float(vType[i]),
	    eta,
	    phi,
	    float(vIso[i]),
	    ETEt,
	    HTEt,
	    ETmissEt,
	    HTmissEt,
	    ETmissPhi,
	    HTmissPhi
	};
    }
};


struct ugmt_hw_data_block{
    std::vector<int> vorbit;
    std::vector<int> vbx;
    std::vector<int> vinterm;
    std::vector<int> vipt;
    std::vector<int> viptunconstrained;
    std::vector<int> vcharge;
    std::vector<int> viso;
    std::vector<int> vindex;
    std::vector<int> vqual;
    std::vector<int> viphi;
    std::vector<int> viphiext;
    std::vector<int> vieta;
    std::vector<int> vietaext;
    std::vector<int> vidxy;
    uint64_t bytes;
    std::vector<int> iAt(int i){
	return {vorbit[i],  
	    vbx[i],     
	    vinterm[i],     
	    vipt[i],    
	    viptunconstrained[i],    
	    vcharge[i], 
	    viso[i],    
	    vindex[i],  
	    vqual[i],   
	    viphi[i],   
	    viphiext[i],
	    vieta[i],   
	    vietaext[i],
	    vidxy[i],
	    bytes,
	};
    }
    std::vector<float> fAt(int i){

	float eta = vieta[i]*gmt_scales::eta_scale;
	float etaext = vietaext[i]*gmt_scales::eta_scale;
	float phi = viphi[i]*gmt_scales::phi_scale;
	float phiext = viphiext[i]*gmt_scales::phi_scale;

	if(phiext>M_PI) phiext=phiext-2*M_PI;
	if(phi>M_PI) phi=phi-2*M_PI;

	float pt  = (vipt[i]-1)*gmt_scales::pt_scale;
	float ptunconstrained  = (viptunconstrained[i]-1)*gmt_scales::ptunconstrained_scale;

	return {float(vorbit[i]),  
	    float(vbx[i]),      
	    float(vinterm[i]),
	    pt,    
	    ptunconstrained,
	    float(vcharge[i]), 
	    float(viso[i]),    
	    float(vindex[i]),  
	    float(vqual[i]),   
	    phi,   
	    phiext,
	    eta,   
	    etaext,
	    float(vidxy[i]),
	    bytes,
	};
    }
    unsigned int size(){return vipt.size();}
    bool empty(){return vipt.empty();}
    static const unsigned int ncols =  15;
    static std::vector<std::string> columns;

};

std::vector<std::string> ugmt_hw_data_block::columns = {"orbit","bx","interm","pt","ptunconstrained","charge","iso","index","qual","phi","phie","eta","etae","dxy","bytes"};

std::vector<std::string> calo_objects_hw_data_block::columns = {"orbit","bx","ET","type","eta","phi","iso", "ETEt", "HTEt", "ETmissEt", "HTmissEt", "ETmissPhi", "HTmissPhi"};

uint64_t skimAndConvertCaloPacked(calo_objects_hw_data_block &retVal, const char* filename, uint32_t start_b, uint32_t end_b){
    uint32_t orbit=0;
    uint32_t bx=0;
    unsigned int count = 0;
    unsigned int countA = 0;
    std::vector<uint32_t> calo_cuts_vect_bx(4,0);
    int ibytes = 0;
    FILE *f=fopen(filename,"r");

    if((end_b - start_b) < 0){
	fprintf(stderr,"processing %s to EOF\n",filename);
    }

    if(f == NULL){
	perror("ERROR opening input file");
	return 0;
    }


    block_calo_packed bl;

    fseek(f , 0 , SEEK_END);
    long lSize = ftell(f);
    rewind(f);
    std::cout << "lSize: " << lSize << std::endl;
    std::cout << "start = " << start_b << std::endl;
    std::cout << "end = " << end_b << std::endl;

    fseek(f, start_b*sizeof(block_calo_packed), SEEK_SET);

    while(ibytes=fread(&bl,1,sizeof(block_calo_packed),f)){

	if(countA > ((lSize-2)/sizeof(block_calo_packed))){
	    std::cout << "eof" << std::endl;
	    fclose(f);
	    return countA;
	}

	if(ferror(f)||feof(f)){
	    std::cout << "eof" << std::endl;
	    fclose(f);
	    return countA;
	}

	orbit = bl.orbit;
	bx = uint32_t(bl.bx);
	std::vector<uint32_t> vET, vEta, vPhi, vIso, vType;
	std::vector<uint32_t> vETEt, vHTEt, vETmissEt, vHTmissEt, vETmissPhi, vHTmissPhi;
	std::vector<bool> calo_cuts_vect(4, 0);
	uint32_t link = 0;
	uint32_t itr_cntr = 0;
	

	for (unsigned int i = 0; i<56; i++){
	    if(i%7 == 0){
		link = bl.frame[i];
		continue;
	    }
		

	    uint32_t ET(0), Eta(0), Phi(0), Iso(0);
	    uint32_t ETEt(0), ETEttem(0), ETMinBiasHF(0), 
		     HTEt(0), HTtowerCount(0), HTMinBiasHF(0), 
		     ETmissEt(0), ETmissPhi(0), ETmissASYMET(0), ETmissMinBiasHF(0), 
		     HTmissEt(0), HTmissPhi(0), HTmissASYMHT(0), HTmissMinBiasHF(0), 
		     ETHFmissEt(0), ETHFmissPhi(0), ETHFmissASYMETHF(0), ETHFmissCENT(0),
		     HTHFmissEt(0), HTHFmissPhi(0), HTHFmissASYMHTHF(0), HTHFmissCENT(0);

	    switch ((link)/2){
		case 1: //egamma
		    {
			ET = ((bl.frame[i] >> shiftsCaloEGamma::ET) & masksCaloEGamma::ET);
			if(ET==0){break;}
			Eta = ((bl.frame[i] >> shiftsCaloEGamma::eta) & masksCaloEGamma::eta);
			Phi = ((bl.frame[i] >> shiftsCaloEGamma::phi) & masksCaloEGamma::phi);
			Iso = ((bl.frame[i] >> shiftsCaloEGamma::iso) & masksCaloEGamma::iso);
			break;
		    }
		case 0: //jets
		    {
			ET = ((bl.frame[i] >> shiftsCaloJet::ET) & masksCaloJet::ET);
			if(ET==0){break;}
			Eta = ((bl.frame[i] >> shiftsCaloJet::eta) & masksCaloJet::eta);
			Phi = ((bl.frame[i] >> shiftsCaloJet::phi) & masksCaloJet::phi);
			Iso = 0; //not defined, but want to use same structure for all three objects
			break;
		    }
		case 3: //taus
		    {
			ET = ((bl.frame[i] >> shiftsCaloTau::ET) & masksCaloTau::ET);
			if(ET==0){break;}
			Eta = ((bl.frame[i] >> shiftsCaloTau::eta) & masksCaloTau::eta);
			Phi = ((bl.frame[i] >> shiftsCaloTau::phi) & masksCaloTau::phi);
			Iso = ((bl.frame[i] >> shiftsCaloTau::iso) & masksCaloTau::iso);
			break;
		    }
		case 2: //energy sums
		    {
			if(link == 4){break;}
			ET = 0; Eta = 0; Phi = 0; Iso = 0;
			itr_cntr++;

			switch(itr_cntr - 1){

			    case 0: //ET
				{
				    ETEt = ((bl.frame[i] >> shiftsCaloESums::ETEt) & masksCaloESums::ETEt);
				    if(ETEt==0){break;}
				    ETEttem = ((bl.frame[i] >> shiftsCaloESums::ETEttem) & masksCaloESums::ETEttem);
				    ETMinBiasHF = ((bl.frame[i] >> shiftsCaloESums::ETMinBiasHF) & masksCaloESums::ETMinBiasHF);
				    break;
				}
			    case 1: //HT
				{
				    HTEt = ((bl.frame[i] >> shiftsCaloESums::HTEt) & masksCaloESums::HTEt);
				    if(HTEt==0){break;}
				    HTtowerCount = ((bl.frame[i] >> shiftsCaloESums::HTtowerCount) & masksCaloESums::HTtowerCount);
				    HTMinBiasHF = ((bl.frame[i] >> shiftsCaloESums::HTMinBiasHF) & masksCaloESums::HTMinBiasHF);

				    break;
				}
			    case 2: //ETMiss
				{
				    ETmissEt = ((bl.frame[i] >> shiftsCaloESums::ETmissEt) & masksCaloESums::ETmissEt);
				    if(ETmissEt==0){break;}
				    ETmissPhi = ((bl.frame[i] >> shiftsCaloESums::ETmissPhi) & masksCaloESums::ETmissPhi);
				    ETmissASYMET = ((bl.frame[i] >> shiftsCaloESums::ETmissASYMET) & masksCaloESums::ETmissASYMET);
				    ETmissMinBiasHF = ((bl.frame[i] >> shiftsCaloESums::ETmissMinBiasHF) & masksCaloESums::ETmissMinBiasHF);
				    break;
				}
			    case 3: //HTMiss
				{
				    HTmissEt = ((bl.frame[i] >> shiftsCaloESums::HTmissEt) & masksCaloESums::HTmissEt);
				    if(HTmissEt==0){break;}
				    HTmissPhi = ((bl.frame[i] >> shiftsCaloESums::HTmissPhi) & masksCaloESums::HTmissPhi);
				    HTmissASYMHT = ((bl.frame[i] >> shiftsCaloESums::HTmissASYMHT) & masksCaloESums::HTmissASYMHT);
				    HTmissMinBiasHF = ((bl.frame[i] >> shiftsCaloESums::HTmissMinBiasHF) & masksCaloESums::HTmissMinBiasHF);
				    break;
				}
			    case 4: //ETHFMiss
				{
				    ETHFmissEt = ((bl.frame[i] >> shiftsCaloESums::ETHFmissEt) & masksCaloESums::ETHFmissEt);
				    if(ETHFmissEt==0){break;}
				    ETHFmissPhi = ((bl.frame[i] >> shiftsCaloESums::ETHFmissPhi) & masksCaloESums::ETHFmissPhi);
				    ETHFmissASYMETHF = ((bl.frame[i] >> shiftsCaloESums::ETHFmissASYMETHF) & masksCaloESums::ETHFmissASYMETHF);
				    ETHFmissCENT = ((bl.frame[i] >> shiftsCaloESums::ETHFmissCENT) & masksCaloESums::ETHFmissCENT);
				    break;
				}
			    case 5: //HTHFMiss
				{
				    HTHFmissEt = ((bl.frame[i] >> shiftsCaloESums::HTHFmissEt) & masksCaloESums::HTHFmissEt);
				    if(HTHFmissEt==0){break;}
				    HTHFmissPhi = ((bl.frame[i] >> shiftsCaloESums::HTHFmissPhi) & masksCaloESums::HTHFmissPhi);
				    HTHFmissASYMHTHF = ((bl.frame[i] >> shiftsCaloESums::HTHFmissASYMHTHF) & masksCaloESums::HTHFmissASYMHTHF);
				    HTHFmissCENT = ((bl.frame[i] >> shiftsCaloESums::HTHFmissCENT) & masksCaloESums::HTHFmissCENT);
				    break;
				}
			    default:
				std::cout << "WARNING:: invalid counter value for ESUMs words, should be (0,5)" << std::endl;
				break;
			}

			break;
		    }
		default:
		    std::cout << "WARNING:: out of range link number" << std::endl;
	    }

	    if((ET != 0) || ((link/2) == 2)){
		vET.push_back(ET);
		vEta.push_back(Eta);
		vPhi.push_back(Phi);
		vIso.push_back(Iso);
		vType.push_back(link/2); 
		vETEt.push_back(ETEt);
		vHTEt.push_back(HTEt);
		vETmissEt.push_back(ETmissEt);
		vHTmissEt.push_back(HTmissEt);
		vETmissPhi.push_back(ETmissPhi);
		vHTmissPhi.push_back(HTmissPhi);
	    }
	    float jetCut = 80;
	    float egammaCut = 20;
	    float tauCut = 80;
	    float esumsCut = 511;

	    if ( ((link/2) == 0) && (ET > jetCut)){
		calo_cuts_vect.at(0) = true;
	    }

	    if ( ((link/2) == 1) && (ET > egammaCut)){
		calo_cuts_vect.at(1) = true;
	    }

	    if ( ((link/2) == 2) && (ETEt > esumsCut)){
		calo_cuts_vect.at(2) = true;
	    }

	    if ( ((link/2) == 3) && (ET > tauCut)){
		calo_cuts_vect.at(3) = true;
	    }

	    if((i == 55) && (vET.size() > 0)){
		for(unsigned int k = 0; k < vET.size(); k++){
		    retVal.vorbit.push_back(int(orbit));
		    retVal.vbx.push_back(int(bx)); 
		    retVal.vET.push_back(vET.at(k)); 
		    retVal.vEta.push_back(vEta.at(k)); 
		    retVal.vPhi.push_back(vPhi.at(k)); 
		    retVal.vIso.push_back(vIso.at(k));
		    retVal.vType.push_back(vType.at(k));
		    retVal.vETEt.push_back(vETEt.at(k));
		    retVal.vHTEt.push_back(vHTEt.at(k));
		    retVal.vETmissEt.push_back(vETmissEt.at(k));
		    retVal.vHTmissEt.push_back(vHTmissEt.at(k));
		    retVal.vETmissPhi.push_back(vETmissPhi.at(k));
		    retVal.vHTmissPhi.push_back(vHTmissPhi.at(k));

		    count++;
		}
	    }

	    if(i == 55){
		vET.clear();
		vEta.clear();
		vPhi.clear();
		vIso.clear();
		vType.clear();
		vETEt.clear();
		vHTEt.clear();
		vETmissEt.clear();
		vHTmissEt.clear();
		vETmissPhi.clear();
		vHTmissPhi.clear();
		for (unsigned int l = 0; l < 4; l++){
		    if(calo_cuts_vect.at(l)){calo_cuts_vect_bx.at(l)++;}
		}
	    }
	}
	countA++;
	if (countA%10000 == 0){std::cout << countA << " blocks unpacked out of " << end_b - start_b << std::endl;}
	if(countA >= end_b - start_b){
	    fprintf(stderr,"max number of blocks reached. Closing input file...\n");
	    for (unsigned int l = 0; l < 4; l++){
		std::cout << "N passing cut at " << l << " = " << calo_cuts_vect_bx.at(l) << std::endl;
	    }
	    fclose(f);
	    return countA;
	}
    }

    fclose(f);
    return countA;
}


uint64_t skimAndConvert(ugmt_hw_data_block &retVal, const char* filename ,uint32_t start_b, uint32_t end_b, int ptcut=0, int qualcut=0, bool excludeIntermediate=true, bool excludeFinal = false){

    uint32_t orbit = 0;
    uint32_t bx = 0;
    uint32_t block_count = 0;
    uint64_t discarded = 0;
    uint64_t full = 0;
    uint64_t eventcount, dimuoncount = 0;
    end_b=end_b*1024;

    FILE *f=fopen(filename,"r");
    if((end_b - start_b) < 0){
	fprintf(stderr,"processing %s to EOF\n",filename);
    }
    if(f == NULL){
	perror("ERROR opening input file");
	return 0;
    }

    block bl;

    int ibytes = 0;
    uint32_t header;
    uint32_t countA = 0; //Nread in bytes
    fseek(f , 0 , SEEK_END);

    long lSize = ftell(f);
    rewind(f);
    std::cout << "lSize: " << lSize << std::endl;
    std::cout << "start = " << start_b << std::endl;
    std::cout << "end = " << end_b << std::endl;

    fseek(f, start_b, SEEK_SET);

    if((countA+start_b) >= lSize){
	std::cout << "eof" << std::endl;
	    std::cout << "dimuoncount "<< dimuoncount << "\n";
	fclose(f);
	return countA;
    }

    while(ibytes=fread(&header,1,sizeof(uint32_t),f)){
	countA += ibytes;	

	if((countA+start_b) >= (lSize- 2048)){
	    std::cout << "eof" << std::endl;
	    std::cout << "dimuoncount "<< dimuoncount << "\n";
	    fclose(f);
	    return countA;
	}
	if((end_b - start_b) < 0){
	    fprintf(stderr,"processing %s to EOF\n",filename);
	}

	if((ferror(f)) || (feof(f))){
	    fclose(f);
	    std::cout << "dimuoncount "<< dimuoncount << "\n";
	    std::cout << "eof" << std::endl; 
	    return countA;
	}

	uint32_t mAcount = (header & packed_format_header_masks::mAcount)>>packed_format_header_shifts::mAcount;

	uint32_t mBcount = (header & packed_format_header_masks::mBcount)>>packed_format_header_shifts::mBcount;

	ibytes = fread(&bl,1,8+(mAcount+mBcount)*12,f);

	countA += ibytes;
	//countA += (8+(mAcount+mBcount)*12);	
	orbit = bl.orbit & 0x7FFFFFFF;
	bx = bl.bx;
	if(mAcount+mBcount==16) full+=1;
	uint32_t dicount = 0;
	/*for(unsigned int i=0;i<mAcount+mBcount; i++){
		
	    uint32_t interm = (bl.mu[i].extra >> shifts::interm) & masks::interm;
		if(interm == 0){
		    dicount++;
		}
	}
	if (dicount <2){continue;}
	dimuoncount++;
	*/for(unsigned int i=0;i<mAcount+mBcount; i++){
	    uint32_t interm = (bl.mu[i].extra >> shifts::interm) & masks::interm;
	    //  std::cout << "bx " << bx << ", orbit " << orbit << ", interm " << interm << std::endl;

	    if(excludeIntermediate && ( interm == 1 )) continue;
	    if(excludeFinal && ( interm == 0 )) continue;
	    // remove intermediate if required 
	    // index==0 and ietaext==0 are a necessary and sufficient condition
	    uint32_t index = (bl.mu[i].s >> shifts::index) & masks::index;
	    uint32_t ietaextu = ((bl.mu[i].f >> shifts::etaext) & masks::etaextv);
	    int32_t ietaext;
	    if(((bl.mu[i].f >> shifts::etaext) & masks::etaexts)!=0){ 
		ietaext = ietaextu -= 256;
	    } else { 
		ietaext = ietaextu;
	    }

	    //extract pt and quality and apply cut if required
	    uint32_t iptunconstrained = (bl.mu[i].s >> shifts::ptuncon) & masks::ptuncon;
	    uint32_t ipt = (bl.mu[i].f >> shifts::pt) & masks::pt;
	    if((ipt-1)<ptcut) {discarded++; continue;}
	    uint32_t qual = (bl.mu[i].f >> shifts::qual) & masks::qual;
	    if(qual < qualcut) {discarded++; continue;}

	    //extract integer value for extrapolated phi 
	    int32_t iphiext = ((bl.mu[i].f >> shifts::phiext) & masks::phiext);

	    //extract integer value for extrapolated phi 
	    int32_t idxy = ((bl.mu[i].s >> shifts::impact) & masks::impact);
	    
	//    if((idxy > 0) && (iptunconstrained > 0)){dimuoncount++;}else{continue;}

	    // extract iso bits and charge
	    uint32_t iso = (bl.mu[i].s >> shifts::iso) & masks::iso;
	    int32_t chrg = 0;
	    if((bl.mu[i].s >> shifts::chrgv) & masks::chrgv==1)
		chrg=(bl.mu[i].s >> shifts::chrg) & masks::chrg==1 ? -1 : 1 ;

	    // extract eta and phi at muon station

	    int32_t iphi = ((bl.mu[i].s >> shifts::phi) & masks::phi);
	    uint32_t ieta1= (bl.mu[i].extra >> shifts::eta1) & masks::eta;
	    uint32_t ieta2= (bl.mu[i].extra >> shifts::eta2) & masks::eta;


	    uint32_t ieta_u;  
	    int32_t ieta;
	    if( (bl.mu[i].extra & 0x1) == 0 ){ieta_u = ieta1; }else{ieta_u = ieta2; } // checking if raw eta should be taken from muon 1 or muon 2

	    //two's complement
	    if ( ieta_u > 256 ){ 
		ieta = ieta_u - 512;
	    }else{ieta = ieta_u;}
	    // push into vectors
	    retVal.vorbit.push_back(orbit);
	    retVal.vbx.push_back(bx); // this is wasteful but gets compressed later in pandas (if used)

	    retVal.vinterm.push_back(interm);
	    retVal.vieta.push_back(ieta);
	    retVal.vietaext.push_back(ietaext);
	    retVal.viphi.push_back(iphi);
	    retVal.viphiext.push_back(iphiext);
	    retVal.vipt.push_back(ipt);
	    retVal.viptunconstrained.push_back(iptunconstrained);
	    retVal.vcharge.push_back(chrg);
	    retVal.vindex.push_back(index);
	    retVal.vqual.push_back(qual);
	    retVal.viso.push_back(iso);
	    retVal.vidxy.push_back(idxy);
	    eventcount++;
	}
	retVal.bytes = ibytes;
	block_count++;

	if((countA + start_b) >= end_b){
	    fprintf(stderr,"reached end of job, closing input file \n");
	    fclose(f);
	    std::cout << "dimuoncount "<< dimuoncount << "\n";
	    return countA;
	}
	if((countA + start_b) >= lSize){
	    fprintf(stderr,"reached end of file, closing input file \n");
	    fclose(f);
	    std::cout << "dimuoncount "<< dimuoncount << "\n";
	    return countA;
	}

	if((end_b - start_b) > 0){
	    if(block_count + start_b >= end_b){
		fprintf(stderr,"max number of blocks reached. Closing input file...\n");
		fclose(f);
	    std::cout << "dimuoncount "<< dimuoncount << "\n";
		return countA;
	    }
	}
	if (countA%100000 == 0){std::cout << countA << " bytes unpacked out of " << end_b - start_b << std::endl;}


	if(block_count%10000000==0){
	    fprintf(stderr,"block %d\n",block_count);
	    fprintf(stderr,"bxs discarded with 0 muons %ld\n",discarded);
	    fprintf(stderr,"bxs with full record %ld\n",full);
	    fprintf(stderr,"muon entries written %ld\n",eventcount);
	}
    }
    fclose(f);
    std::cout << "dimuoncount "<< dimuoncount << "\n";
    return countA;
}


extern "C" PyObject*  makeConvertToList(const char *filename, int start_b, int end_b, int ptcut=0, int qualcut=0, bool excludeIntermediate = true, bool excludeFinal = false, bool wantFloats = true){

    fprintf(stderr,"makeConvertToList called with args: filename=%s start_b=%d end_b=%d ptcut=%d qualcut=%d excludeIntermediate=%d excludeFinal=%d wantFloats=%d \n",filename,start_b,end_b,ptcut,qualcut,excludeIntermediate,excludeFinal,wantFloats);

    ugmt_hw_data_block data;
    try{
	uint64_t counts = skimAndConvert(data, filename, start_b, end_b, ptcut, qualcut, excludeIntermediate, excludeFinal);}
    catch (const std::exception& e){}

    if(data.empty()){
	fprintf(stderr,"WARNING: Input file %s could not be converted\n\n",filename);
	return PyList_New(0);
    }

    PyObject* result = PyList_New(data.size()); 
    PyGILState_STATE gstate = PyGILState_Ensure();

    for(unsigned int i = 0; i < data.size(); i++){
	PyObject* line = PyList_New(data.ncols);
	fprintf(stderr,"at row %d\n",i);
	if(wantFloats){
	    for(unsigned int j = 0; j < data.ncols; j++){
		PyList_SetItem(line,j,PyFloat_FromDouble(data.fAt(i)[j]));
	    }
	}else{
	    for(unsigned int j = 0; j < data.ncols; j++){
		PyList_SetItem(line,j,PyLong_FromLong(data.iAt(i)[j]));
	    }
	}
	int ret = PyList_SetItem(result,i,line);
	if(ret!=0) fprintf(stderr,"error setting item at %d\n",i);
	//    Py_DECREF(line); //PyList_SetItem takes ownership of reference 

    }
    PyGILState_Release(gstate);

    return result;
}

extern "C" PyObject*  makeConvertToDictCalo(const char *filename, int start_b, int end_b, bool wantFloats){

    fprintf(stderr,"makeConvertToDict called with args: filename=%s start_b=%d end_b=%d wantFloats=%d \n",filename,start_b,end_b,wantFloats);

    calo_objects_hw_data_block data;
    try{
	uint64_t counts = skimAndConvertCaloPacked(data, filename, start_b, end_b);
    }catch (const std::exception& e){std::cout << "CALO: file end failure" << std::endl;}
    if(data.empty()){
	fprintf(stderr,"Input file %s could not be converted",filename);
	return PyList_New(0);
    }

    PyObject* result = PyDict_New(); 

    PyGILState_STATE gstate = PyGILState_Ensure();
    std::cout << "nEvents = " << data.size() << std::endl;

    for(unsigned int i = 0; i < data.ncols; i++){
	fprintf(stderr,"at column %d\n",i);
	PyObject* line = PyList_New(data.size());
	if(wantFloats){
	    for(unsigned int j = 0; j < data.size(); j++){//This is slow, can we speed it up?
		PyList_SetItem(line,j,PyFloat_FromDouble(data.fAt(j)[i]));
	    }
	}else{
	    for(unsigned int j = 0; j < data.size(); j++){
		PyList_SetItem(line,j,PyLong_FromLong(data.iAt(j)[i]));
	    }
	}
	PyDict_SetItemString(result,data.columns[i].c_str(),line);
	Py_DECREF(line);
    }
    PyGILState_Release(gstate);

    return result;
}
extern "C" PyObject*  makeConvertToDict(const char *filename, int start_b, int end_b, int ptcut=0, int qualcut=0, bool excludeIntermediate = true, bool excludeFinal = false, bool wantFloats = true){

    fprintf(stderr,"makeConvertToDict called with args: filename=%s start_b=%d end_b=%d ptcut=%d qualcut=%d excludeIntermediate=%d excludeFinal=%d wantFloats=%d \n",filename,start_b,end_b,ptcut,qualcut,excludeIntermediate,excludeFinal,wantFloats);

    ugmt_hw_data_block data;
    try{
	uint64_t counts = skimAndConvert(data, filename, start_b, end_b, ptcut, qualcut, excludeIntermediate, excludeFinal);
    }catch (const std::exception& e){std::cout << "GMT: file end failure" << std::endl;}

    if(data.empty()){
	fprintf(stderr,"Input file %s could not be converted",filename);
	return PyList_New(0);
    }

    PyObject* result = PyDict_New(); 

    PyGILState_STATE gstate = PyGILState_Ensure();

    for(unsigned int i = 0; i < data.ncols; i++){
	fprintf(stderr,"at column %d\n",i);
	PyObject* line = PyList_New(data.size());
	if(wantFloats){
	    for(unsigned int j = 0; j < data.size(); j++){
		PyList_SetItem(line,j,PyFloat_FromDouble(data.fAt(j)[i]));
	    }
	}else{
	    for(unsigned int j = 0; j < data.size(); j++){
		PyList_SetItem(line,j,PyLong_FromLong(data.iAt(j)[i]));
	    }
	}
	PyDict_SetItemString(result,data.columns[i].c_str(),line);
	Py_DECREF(line);
    }
    PyGILState_Release(gstate);

    return result;
}

static PyObject* getListOfVariables(PyObject *self, PyObject *args){
    std::vector<std::string> cols = ugmt_hw_data_block::columns;
    PyObject* result = PyList_New(cols.size());
    for(unsigned int i = 0; i < cols.size(); i++){
	PyList_SetItem(result,i,PyUnicode_FromString(cols[i].c_str()));
    }
    return result;
}

static PyObject*  makeConvertCalo(PyObject *self, PyObject *args, PyObject *keywds){
    const char * filename = "superpippo";
    int wantDict=true;
    int wantFloats;
    int start_b;
    int end_b;

    char * kwlist[] = {(char*)"",(char*)"",(char*)"",(char*)"wantDict", (char*)"wantFloats", NULL};

    if (!PyArg_ParseTupleAndKeywords(args, keywds, "si|iii$pp", kwlist, &filename,&start_b,&end_b,&wantDict,&wantFloats))
	return NULL;

    if(wantDict){
	return makeConvertToDictCalo(filename, start_b, end_b, wantFloats);    
    }else{
	std::cout << " Convert to List is not currently supported for Calo data " << std::endl;
	return 0;
    }
}

static PyObject*  makeConvert(PyObject *self, PyObject *args, PyObject *keywds){
    const char * filename = "superpippo";
    int ptcut=0;
    int qualcut=0;
    int excludeIntermediate=true;
    int excludeFinal=false;
    int wantDict=false;
    int wantFloats=true;
    int start_b;
    int end_b;
    //char * kwlist[] = {(char*)"",(char*)"",(char*)"",(char*)"",(char*)"excludeIntermediate",(char*)"excludeFinal", (char*)"wantDict", (char*)"wantFloats", NULL};
    char * kwlist[] = {(char*)"",(char*)"",(char*)"",(char*)"",(char*)"",(char*)"excludeIntermediate",(char*)"excludeFinal",(char*)"wantDict", (char*)"wantFloats", NULL};

    if (!PyArg_ParseTupleAndKeywords(args, keywds, "si|iii$pppp", kwlist, &filename,&start_b,&end_b,&ptcut,&qualcut,&excludeIntermediate,&excludeFinal,&wantDict,&wantFloats))
	return NULL;

    if(wantDict){
	return makeConvertToDict(filename,start_b,end_b,ptcut,qualcut,excludeIntermediate,excludeFinal,wantFloats);    
    }else{
	return makeConvertToList(filename,start_b,end_b,ptcut,qualcut,excludeIntermediate,excludeFinal,wantFloats);    
    }
}


static PyMethodDef SkacMethods[] = {
    {"convert",  (PyCFunction)(void(*)(void))makeConvert, METH_VARARGS | METH_KEYWORDS,
	"convert raw file to python dict of lists (json/numpy/pandas) or list of lists (csv)."},
    {"convertcalo",  (PyCFunction)(void(*)(void))makeConvertCalo, METH_VARARGS | METH_KEYWORDS,
	"convert raw file to python dict of lists (json/numpy/pandas) or list of lists (csv)."},
    {"getColumns", getListOfVariables, METH_VARARGS, "get ordered list of variable names."},
    {NULL, NULL, 0, NULL}        /* Sentinel */
};


static struct PyModuleDef scunpackmodule = {
    PyModuleDef_HEAD_INIT,
    "scunpack",   /* name of module */
    NULL, /* module documentation, may be NULL */
    -1,       /* size of per-interpreter state of the module,
		 or -1 if the module keeps state in global variables. */
    SkacMethods
};


    PyMODINIT_FUNC
PyInit_scunpack(void)
{
    return PyModule_Create(&scunpackmodule);
}
