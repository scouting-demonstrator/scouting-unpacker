#include <iostream>
#include <iomanip>
#include <cstdio>
#include <cstdint>
#include <getopt.h>
#include <string>

#include <arrow/api.h>
#include <arrow/io/api.h>
#include <parquet/arrow/writer.h>
#include <parquet/stream_writer.h>
#include <arrow/ipc/api.h>
#include <parquet/arrow/reader.h>
#include <parquet/arrow/writer.h>

// #include "parquet/stream_writer.h"

#include "scouting-headers/arrow_blocks.h"
#include "scouting-headers/arrow_schemas.h"
#include "converters.h"
#include "tools.h"

#define NBLOCKS_DEFAULT (-1)
#define NPQTROWS_DEFAULT (50000000)





static struct option const long_opts[] = {
    // {"source",   required_argument, NULL, 's'},
    {"ifile",    required_argument, NULL, 'i'},
    {"nblocks",  required_argument, NULL, 'b'},
    {"npqtrows", required_argument, NULL, 'r'},
    {"help",     no_argument,       NULL, 'h'},
    {"verbose",  no_argument,       NULL, 'v'},
    {0, 0, 0, 0}
};




void usage(const char *name)
{
    int i = 0;
    fprintf(stdout, "%s\n\n", name);
    fprintf(stdout, "usage: %s [OPTIONS]\n\n", name);
    fprintf(stdout, "Run L1 Scouting raw data unpacker\n\n");

    // fprintf(stdout, "  -%c (--%s) L1 trigger source (required)\n",
    //     long_opts[i].val, long_opts[i].name);
    // i++;
    fprintf(stdout, "  -%c (--%s) Input filename to unpack (required)\n",
        long_opts[i].val, long_opts[i].name);
    i++;
    fprintf(stdout, "  -%c (--%s) Maximum number of blocks to unpack (default: %d.)\n",
           long_opts[i].val, long_opts[i].name, NBLOCKS_DEFAULT);
    i++;
    fprintf(stdout, "  -%c (--%s) Number of rows per chunk in output parquet file (default: %d.)\n",
           long_opts[i].val, long_opts[i].name, NPQTROWS_DEFAULT);
    i++;
    fprintf(stdout, "  -%c (--%s) Print usage help and exit\n",
        long_opts[i].val, long_opts[i].name);
    i++;
    fprintf(stdout, "  -%c (--%s) Verbose output\n",
        long_opts[i].val, long_opts[i].name);
    i++;
}





int main(int argc, char** argv) {

    int cmd_opt;
    // char *source;
    char *filename;
    int64_t nblocks   = NBLOCKS_DEFAULT;
    uint64_t npqtrows = NPQTROWS_DEFAULT;
    bool verbose      = false;
    while ((cmd_opt = getopt_long(argc, argv, "vh:i:b:r:", long_opts, NULL)) != -1) {
        switch (cmd_opt) {
            case 0:
                /* long option */
                break;
            // case 's':
            //     // source [GMT|CALO]
            //     source = strdup(optarg);
            //     break;
            case 'i':
                // input filename
                filename = strdup(optarg);
                break;
            case 'b':
                /* RAM address on the AXI bus in bytes */
                nblocks = getopt_integer(optarg);
                break;
            case 'r':
                /* memory aperture windows size */
                npqtrows = getopt_integer(optarg);
                break;
            case 'v':
                verbose = 1;
                break;
            case 'h':
            default:
                usage(argv[0]);
                exit(0);
                break;
            }
    }

    // create strings for input/output file names
    std::string ofilename = filename;
    std::string ifilename = filename;

    // add Parquet extension to output file name string
    ofilename = ofilename.substr(0, ofilename.find_last_of('.'))+".pqt";

    // find L1 source from input filename and apply converter
    if (ifilename.find("GMT") != std::string::npos) {
        ugmt::arrow_data_block adblock;
        ugmt::skimAndConvert(adblock, ifilename.c_str(), ofilename.c_str(), nblocks, npqtrows);
        std::shared_ptr<arrow::Table> table = arrow::Table::Make(ugmt::schema, adblock.arrayData);
        write_parquet_file(*table, ofilename.c_str(), npqtrows);
    }
    else if (ifilename.find("CALO") != std::string::npos) {
        demux::arrow_data_block adblock;
        demux::skimAndConvert(adblock, ifilename.c_str(), ofilename.c_str(), nblocks, npqtrows);
        std::shared_ptr<arrow::Table> table = arrow::Table::Make(demux::schema, adblock.arrayData);
        write_parquet_file(*table, ofilename.c_str(), npqtrows);
    }
    else if (ifilename.find("GT") != std::string::npos) {
        ugt::arrow_data_block adblock;
        ugt::skimAndConvert(adblock, ifilename.c_str(), ofilename.c_str(), nblocks, npqtrows);
        std::shared_ptr<arrow::Table> table = arrow::Table::Make(ugt::schema, adblock.arrayData);
        write_parquet_file(*table, ofilename.c_str(), npqtrows);
    }
    else {
        std::cerr << "Source not recognized or not supported..." << std::endl;
    }

}
