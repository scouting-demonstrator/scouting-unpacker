#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include "include/Orbit.h"

namespace ugmt{

  Orbit convert(unsigned char *&buf, size_t size);
}

#endif
