#ifndef BUNCHCROSSING_H
#define BUNCHCROSSING_H
#include <vector>
#include <cstdint>
#include <cstring>
#include "include/low-level.h"
namespace ugmt{
  class BunchCrossing{
  public:
    BunchCrossing(const uint16_t _bx, const unsigned int nmuons, const format::muon *_muons) : bx(_bx), muons(nmuons) {
      memcpy(&muons[0],_muons,sizeof(ugmt::format::muon)*nmuons);
    }
    const uint16_t getBx(){return bx;}
    const std::vector<format::muon>& getMuons(){return muons;}
    const std::vector<format::muon> getIntermediateMuons()
    {
      std::vector<format::muon> retVal;
      for(auto i : muons){
      }
      return retVal;
    }
    const std::vector<format::muon> getFinalMuons()
    {
      std::vector<format::muon> retVal;
      for(auto i : muons){
      }
    return retVal;
    }
    
  private:
    uint16_t bx;
    std::vector<format::muon> muons;
  };
}
#endif
