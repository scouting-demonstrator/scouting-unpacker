#ifndef TOOLS_H
#define TOOLS_H

#include <cstdint>
#include <arrow/api.h>



// Write out an arrow Table as a Parquet file
void write_parquet_file(const arrow::Table& table, const char *ofilename, uint32_t npqtrows);



// get cmd line int argument
uint64_t getopt_integer(char *optarg);



// Loops over each word in the orbit trailer BX map and fills a vector with the non-empty BX values
void bit_check(std::vector<uint16_t> *bx_vect, uint32_t word, uint32_t offset);

#endif