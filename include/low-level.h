#ifndef LOWLEVEL_H
#define LOWLEVEL_H

#include <cstdint>


namespace ugmt{
  namespace format{

    struct muon{
      uint32_t f;
      uint32_t s;
    };
    
    struct bunch_crossing{
      uint32_t bx;
      uint32_t orbit;
      muon mu[16];
    };}
}
#endif

