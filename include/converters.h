#ifndef CONVERTERS_H
#define CONVERTERS_H

#include "tools.h"
#include "scouting-headers/arrow_blocks.h"
#include <cstdint>



namespace ugmt {
    uint64_t skimAndConvert(ugmt::arrow_data_block &adblock,
                            const char *filename,
                            const char *ofilename,
                            int64_t nblocks,
                            uint32_t npqtrows,
                            int ptcut=0,
                            int qualcut=0,
                            bool excludeIntermediate=true,
                            bool excludeFinal=false);
}



namespace demux {
    uint64_t skimAndConvert(demux::arrow_data_block &adblock,
                            const char *filename,
                            const char *ofilename,
                            int64_t nblocks,
                            uint32_t npqtrows);
}



namespace ugt {
    uint64_t skimAndConvert(ugt::arrow_data_block &adblock,
                            const char *filename,
                            const char *ofilename,
                            int64_t nblocks,
                            uint32_t npqtrows);
}

#endif