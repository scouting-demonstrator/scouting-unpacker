#ifndef BUILDER_H
#define BUILDER_H

#include <vector>
#include <arrow/api.h>


namespace ugmt{
  class Orbit;
  class builder{
  public:
    
    builder(std::shared_ptr<arrow::DataType>, std::shared_ptr<arrow::DataType>);
    arrow::Status append(Orbit &o);
    std::shared_ptr<arrow::Table> finish(std::shared_ptr<arrow::Schema> schema);
    
  private:
    
    arrow::MemoryPool* pool;
    std::shared_ptr<arrow::UInt32Builder> f_builder; ;
    std::shared_ptr<arrow::UInt32Builder> s_builder; 
    std::vector<std::shared_ptr<arrow::ArrayBuilder> > muon_field_builders; 
    std::shared_ptr<arrow::StructBuilder> muon_builder;
    
    std::shared_ptr<arrow::ListBuilder> muons_builder;
    std::shared_ptr<arrow::UInt32Builder> bxn_builder;
    std::vector<std::shared_ptr<arrow::ArrayBuilder> > bunch_crossing_component_builders;
    std::shared_ptr<arrow::StructBuilder> bunch_crossing_builder;
    std::shared_ptr<arrow::ListBuilder> bx_list_builder;
    std::shared_ptr<arrow::UInt32Builder> orbitn_builder;
    
  };
}



#endif
