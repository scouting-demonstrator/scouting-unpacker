#ifndef FORMAT_H
#define FORMAT_H

#include <cstdint>

namespace ugmt{
  namespace format{
    
    struct masks{
      static constexpr  uint32_t phiext = 0x3ff;     //10 bits 0-2*pi in steps of 2.*M_PI/576.
      static constexpr  uint32_t pt = 0x1ff;         //9 bits
      static constexpr  uint32_t qual = 0xf;         //4 bits 
      static constexpr  uint32_t etaext = 0x1ff;     //9 bits (2's complement)
      static constexpr  uint32_t etaextv = 0xff;     //8 bits value
      static constexpr  uint32_t etaexts = 0x100;    //1 bit sign (2's complement)
      static constexpr  uint32_t iso = 0x3;          //2 bits, actually unused
      static constexpr  uint32_t chrg = 0x1;         //1 bit: convention 1:= "-" ,0:= "+"
      static constexpr  uint32_t chrgv = 0x1;        //1 bit charge valid
      static constexpr  uint32_t index = 0x7f;       //7 bits
      static constexpr  uint32_t phi = 0x3ff;        //10 bits 0-2*pi
      static constexpr  uint32_t eta = 0x1ff;        //9 bits (2's complement)	  
      static constexpr  uint32_t etav = 0xff;	     //8 bits value		  
      static constexpr  uint32_t etas = 0x100;	     //1 bit sign (2's complement)
      static constexpr  uint32_t rsv = 0x3;          //2 bits reserved in version for Run-2
    };

    struct shifts{
      static constexpr  uint32_t phiext = 0;
      static constexpr  uint32_t pt = 10;
      static constexpr  uint32_t qual = 19;
      static constexpr  uint32_t etaext = 23;
      static constexpr  uint32_t iso =	0;
      static constexpr  uint32_t chrg = 2;
      static constexpr  uint32_t chrgv	= 3;
      static constexpr  uint32_t index	= 4;
      static constexpr  uint32_t phi = 11;
      static constexpr  uint32_t eta =	21;
      static constexpr  uint32_t rsv =	30;
    };

    struct header_shifts{
      static constexpr uint32_t bxmatch=24;
      static constexpr uint32_t mAcount=16;
      static constexpr uint32_t orbitmatch=8;
      static constexpr uint32_t mBcount=0;
    };

    struct header_masks{
      static constexpr uint32_t bxmatch = 0xff<<header_shifts::bxmatch;
      static constexpr uint32_t mAcount = 0xf<<header_shifts::mAcount;
      static constexpr uint32_t orbitmatch = 0xff<<header_shifts::orbitmatch;
      static constexpr uint32_t mBcount = 0xf;
    };

  }
}
#endif
