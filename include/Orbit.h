#ifndef ORBIT_H
#define ORBIT_H

#include <vector>
#include "include/BunchCrossing.h"


namespace ugmt{
  class Orbit{
  public:
    Orbit(uint32_t _orbit, const std::vector<BunchCrossing> _bxv) : orbit(_orbit), bxv(_bxv){}
    const uint32_t &getOrbit(){return orbit;}
    const std::vector<BunchCrossing> &bunchCrossings(){ return bxv;}
  private:
    uint32_t orbit;
    std::vector<BunchCrossing> bxv;
  };
}
#endif
