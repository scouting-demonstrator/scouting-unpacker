# Scouting Unpacker

This repository contains the source code, scripts and all the other utilities to unpack the RAW L1 Scouting data. Arrow and Parquet libraries are needed to run the application. The unpacker writes the output files in Parquet format.



## How to run

Clone the repository (and all the submodules) using:
```
git clone --recurse-submodules ssh://git@gitlab.cern.ch:7999/scouting-demonstrator/scouting-unpacker.git -b reformat-parquet
```

Assuming that the Apache Arrow and Parquet libraries (v10) have been correctly installed, compile the application using:
```
cd scouting-unpacker/src/
cmake .
make
```

Run the application:
```
./scunpack [-h] -s [GMT|CALO|GT] -i INPUT_FILE -b NBLOCKS -r PARQUETROWS
```



## Installation using RPM

Please create the RPM using scripts/scunpackrpm.sh. The RPM can then be installed as usual.

Service is called runSCunpack and can be controlled in the usual way. Logs go to journalctl.

<h2>Moving files</h2>

Files can be moved from CMS P5 with scripts/filemover.sh

<h2>Scripts to generate DQM plots</h2>

./scripts/scoutDQM_GMT_Calo.sh is the recommended way to generate plots for the DQM. The plots will live inside the plots sub-directory and be served with 'sigal'.

<h2>Python binding</h2>

usage:

`getColumns()`

returns a list of the column names corresponding to the return list of `convert` with `wantDict=False` (see below)

`convert(filename,nev,ptcut[=0],qualcut[=0])`

nev = max number of bunch-crossings to convert (will return less if EOF is encountered, set to -1 to convert the whole file)

ptcut = integer value, in steps of 0.5 GeV, applied to the raw Pt from GMT (default no cut)

qualcut = minimum quality required (default 0)


`from skac.skac import *`

`adictionary = convert("../test/scout_325159_000016.dat",10,excludeIntermediate=True,wantDict=True,wantFloats=True)`

will create a dictionary with 100k "bunch crossings" read from the file, excluding intermediate muons and with the "float" scale factors applied.

The dictionary can be directly stored to a json file:

`import json`

`f=open('afile.json','w')`

`json.dump(adictionary,f)`

`f.close()`

Or converted to a pandas dataframe:


`import pandas as pd`

`pd.DataFrame(adictionary)`

<table border="1" class="dataframe">
	<thead>
		<tr style="text-align: right;">
		    <th></th>      <th>orbit</th>      <th>bx</th>      <th>pt</th>      <th>charge</th>      <th>iso</th>
		    		   <th>index</th>      <th>qual</th>      <th>phi</th>      <th>phie</th>      <th>eta</th>
				   <th>etae</th>
	        </tr>
	</thead>
	<tbody>
		<tr>
		<th>0</th>      <td>33687772.0</td>      <td>76.0</td>      <td>35.0</td>      <td>1.0</td>      <td>0.0</td>      <td>84.0</td>      <td>12.0</td>      <td>-1.767146</td>      <td>-1.723513</td>      <td>-1.120125</td>      <td>-1.120125</td>    </tr>    <tr>
		<th>1</th>      <td>33687772.0</td>      <td>81.0</td>      <td>2.5</td>      <td>-1.0</td>      <td>0.0</td>      <td>93.0</td>      <td>12.0</td>      <td>2.148937</td>      <td>1.712604</td>      <td>-2.011875</td>      <td>-1.979250</td>    </tr>    <tr>
		<th>2</th>      <td>33687772.0</td>      <td>84.0</td>      <td>6.0</td>      <td>-1.0</td>      <td>0.0</td>      <td>6.0</td>      <td>12.0</td>      <td>2.585269</td>      <td>2.323469</td>      <td>1.794375</td>      <td>1.783500</td>    </tr>    <tr>
		<th>3</th>      <td>33687772.0</td>      <td>91.0</td>      <td>16.0</td>      <td>1.0</td>      <td>0.0</td>      <td>81.0</td>      <td>0.0</td>      <td>-2.748893</td>      <td>-2.574361</td>      <td>-1.196250</td>      <td>-1.185375</td>    </tr>    <tr>
		<th>4</th>      <td>33687772.0</td>      <td>92.0</td>      <td>2.5</td>      <td>1.0</td>      <td>0.0</td>      <td>96.0</td>      <td>8.0</td>      <td>-3.097960</td>      <td>-2.748893</td>      <td>-2.240250</td>      <td>-2.218500</td>    </tr>    <tr>
		<th>5</th>      <td>33687772.0</td>      <td>106.0</td>      <td>2.0</td>      <td>-1.0</td>      <td>0.0</td>      <td>90.0</td>      <td>12.0</td>      <td>1.014473</td>      <td>0.621774</td>      <td>-2.175000</td>      <td>-2.142375</td>    </tr>    <tr>
		<th>6</th>      <td>33687772.0</td>      <td>127.0</td>      <td>2.5</td>      <td>1.0</td>      <td>0.0</td>      <td>96.0</td>      <td>12.0</td>      <td>2.356194</td>      <td>2.748893</td>      <td>-2.088000</td>      <td>-2.055375</td>    </tr>    <tr>
		<th>7</th>      <td>33687772.0</td>      <td>138.0</td>      <td>2.0</td>      <td>-1.0</td>      <td>0.0</td>      <td>90.0</td>      <td>4.0</td>      <td>0.261799</td>      <td>-0.261800</td>      <td>-2.066250</td>      <td>-2.022750</td>    </tr>    <tr>
		<th>8</th>      <td>33687772.0</td>      <td>152.0</td>      <td>2.0</td>      <td>-1.0</td>      <td>0.0</td>      <td>99.0</td>      <td>4.0</td>      <td>-2.607086</td>      <td>-2.999785</td>      <td>-2.175000</td>      <td>-2.142375</td>    </tr>    <tr>
		<th>9</th>      <td>33687772.0</td>      <td>160.0</td>      <td>4.0</td>      <td>1.0</td>      <td>0.0</td>      <td>96.0</td>      <td>12.0</td>      <td>2.912518</td>      <td>3.130684</td>      <td>-2.196750</td>      <td>-2.185875</td>    </tr>
	</tbody>
</table>


kwargs:

- `excludeIntermediate = False` (default True) will include also intermediate muons
- `wantFloats = False` (default True) will produce integer values directly from unpacking (eta sign is correct)
- `wantDict = False` (default False) will produce a list of lists where each element in the top list is a "muon"
